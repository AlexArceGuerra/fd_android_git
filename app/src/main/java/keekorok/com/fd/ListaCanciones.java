package keekorok.com.fd;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Environment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;


import com.rtve.stats.StatsManage;
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import keekorok.com.fd.Adapter.CustomItemClickListener;
import keekorok.com.fd.Adapter.ListaCancionesAdapter;
import keekorok.com.fd.Common.StorageUtils;
import keekorok.com.fd.Modelos.Cancion;
import keekorok.com.fd.Modelos.ExistVideo;
import keekorok.com.fd.Modelos.ExistVideoResponse;
import keekorok.com.fd.Modelos.ListaCancionesResponse;
import keekorok.com.fd.Modelos.LoginResponseDatos;
import keekorok.com.fd.Modelos.RequestInterface;
import keekorok.com.fd.Modelos.Utils;
import keekorok.com.fd.RestService.ApiCliente;
import keekorok.com.fd.Utilidades.AppError;
import keekorok.com.fd.Utilidades.StatsManagerHelper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ListaCanciones extends AppCompatActivity implements View.OnClickListener{

    public Utils mainModel = Utils.getInstance();
    private RecyclerView recycler;
    private ListaCancionesAdapter adapter;
    private ImageButton icono_atras;
    private ProgressBar mProgressBar;
    private List<Cancion> arrayCanciones = new ArrayList<>();
    String identificadorUsuario;
    private MediaPlayer mediaPlayer;
    private LinearLayout lLayout;
    private ImageButton botonPulsado;
    private ListaCancionesAdapter.CancionViewHolder holder;
    private boolean playing;
    private SharedPreferences prefs;
    private File folder;
    private File fileArtist;
    private File fileText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //Modificacion Bitbucket
        super.onCreate(savedInstanceState);
        mainModel.activity = this;
        setContentView(R.layout.listado_canciones);
        prefs=getSharedPreferences(getString(R.string.preferences), Context.MODE_PRIVATE);
        prefs.edit().putBoolean("desfasealertmostrada", false).apply();
        Intent intent = getIntent();
        identificadorUsuario = (String) intent.getSerializableExtra("idUsuario");
        deleteFiles();
        if (!mainModel.initialized) {
            mainModel.initialize(getApplicationContext(), this);

        }

        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //configurarActionBar();
        configurarVista();
        StatsManagerHelper.initStatsManager(getApplication(), getApplicationContext());
        StatsManagerHelper.sendView(StatsManagerHelper.Pantalla_Lista);
        RequestInterface serviceListaCanciones = ApiCliente.getService().create(RequestInterface.class);
        Call<ListaCancionesResponse> request = serviceListaCanciones.getListaCanciones("","android");


        request.enqueue(new Callback<ListaCancionesResponse>() {
            @Override
            public void onResponse(Call<ListaCancionesResponse> call, Response<ListaCancionesResponse> response) {
                if (!response.isSuccess()) {

                    Log.i("FD","Error: "+response.code());

                }else {

                    ListaCancionesResponse lista = response.body();

                    Log.i("FD", "Codigo Respuesta " +response.code());

                    for (Cancion cancion : lista.canciones){

                        if(!cancion.getVar2().equals("") && !cancion.getVar2().equals(" ")){
                            cancion.setUrlVideoTexto(cancion.getVar2());
                            cancion.setDescargarTexto(true);
                            cancion.setUrlVideoBase(cancion.getVar1());
                        }else{
                            cancion.setDescargarTexto(false);
                        }
                        arrayCanciones.add(cancion);
                        adapter.addCancion(cancion);

                    }


                }

            }

            @Override
            public void onFailure(Call<ListaCancionesResponse> call, Throwable t) {

                Log.i("FD","Error: " +t.getMessage());

            }
        });

    }

    private void deleteFiles() {
        if(fileArtist!=null){
            if(fileArtist.exists()){
                fileArtist.delete();
            }
        }else{
            if(folder==null){
                folder=new File(Environment.getExternalStorageDirectory().toString() + "/.FDVIDEOS/");
            }
            fileArtist=new File(folder,".videopruebaartista.mp4");
            if(fileArtist.exists()){
                fileArtist.delete();
            }
        }
        if(fileText!=null){
            if(fileText.exists()){
                fileText.delete();
            }
        }else{
            if(folder==null){
                folder=new File(Environment.getExternalStorageDirectory().toString() + "/.FDVIDEOS/");
            }
            fileText=new File(folder, ".videotext.mp4");
            if(fileText.exists()){
                fileText.delete();
            }
        }

    }

    /*
    private void configurarActionBar() {

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setIcon(R.mipmap.ic_launcher);

    }
    */
    private void configurarVista() {
        icono_atras = (ImageButton) findViewById(R.id.icono_atras);
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);
        recycler = (RecyclerView) findViewById(R.id.listado);
        recycler.setHasFixedSize(true);
        recycler.addItemDecoration(new HorizontalDividerItemDecoration.Builder(this).build());
        recycler.setLayoutManager(new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.VERTICAL,false));

        adapter = new ListaCancionesAdapter(getApplicationContext(), new CustomItemClickListener() {
            @Override
            public void onItemClick(View v, int position,MediaPlayer reproductor) {
                Log.d("FD", "clicked position:" + position);
                String autorClicado = arrayCanciones.get(position).getAutor();
                Log.d("FD", "autor pulsado:" + autorClicado);

                comprobarBotonPulsado(v,position,reproductor);

            }
        });

        recycler.setAdapter(adapter);
        icono_atras.setOnClickListener(this);

        lLayout=(LinearLayout)findViewById(R.id.activity_login);
        lLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                /*if(botonPulsado!=null){
                    Log.i("David", "OnTouchListener llamado");
                    comprobarSiAudioEstaActivo();

                    ListaCancionesAdapter.CancionViewHolder myHolder=(ListaCancionesAdapter.CancionViewHolder) adapter.getHolder();
                    myHolder.botonReproducirAudio.setImageResource(R.drawable.circled_play_filled);
                    myHolder.botonCantar.setEnabled(true);
                    myHolder.botonCantar.setAlpha(1f);
                    adapter.notifyDataSetChanged();
                }*/

                return false;
            }
        });
        recycler.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                /*if(botonPulsado!=null){
                    Log.i("David", "OnTouchListener del recycler llamado");
                    comprobarSiAudioEstaActivo();
                    //botonPulsado.setImageResource(R.drawable.circled_play_filled);
                    ListaCancionesAdapter.CancionViewHolder myHolder=(ListaCancionesAdapter.CancionViewHolder) adapter.getHolder();
                    myHolder.botonReproducirAudio.setImageResource(R.drawable.circled_play_filled);
                    myHolder.botonCantar.setEnabled(true);
                    myHolder.botonCantar.setAlpha(1f);
                    adapter.notifyDataSetChanged();
                }*/

                return false;
            }
        });
        /*lLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("David", "Hemos tocado la pantalla. Paramos la reproducción, si se está produciendo");
                comprobarSiAudioEstaActivo();
            }
        });*/
    }

    //*********************** Listener Botones ****************************
    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.icono_atras:
                volverAtras();
                break;

            default:
                break;
        }

    }


    public void comprobarBotonPulsado(View v, int position, MediaPlayer reproductor) {

        Log.i("David", "Método comprobarBotonPulsado llamado");

        switch (v.getId()) {

            case R.id.btnCantar:

                if(prefs.getBoolean("gotosing", true)){
                    //Se comprueba si ya existe el video
                    deleteFiles();
                    Cancion cancionSeleccionada = arrayCanciones.get(position);
                    prefs.edit().putBoolean("primerapulsacion", true).apply();
                    comprobarSiExisteVideo(cancionSeleccionada);
                }


                break;

            case R.id.btnReproducirAudio:

                Log.i("David", "Boton reproducir audio pulsado");
            if(holder==null){
                holder=(ListaCancionesAdapter.CancionViewHolder)adapter.getHolder();
            }
                if(holder.isPlaying()){
                    mediaPlayer.stop();
                    holder.botonCantar.setEnabled(true);
                    holder.botonCantar.setAlpha(1f);
                }else{
                    mediaPlayer = reproductor;
                    botonPulsado=(ImageButton)v;
                    //comprobarSiAudioEstaActivo();
                    //checkAudio(mediaPlayer);
                }

                break;

            default:
                break;
        }

    }

    private void checkAudio(MediaPlayer mediaPlayer) {

        //mediaPlayer.stop();
    }

    @Override
    public void startActivity(Intent intent) {
        overridePendingTransition(0, 0);
        super.startActivity(intent);
        overridePendingTransition(0, 0);
    }

    @Override
    protected void onDestroy() {
        StatsManage.appClose();
        super.onDestroy();

    }





    /*@Override
    public void onBackPressed()
    {
        return;
    }*/

    public void volverAtras(){

        comprobarSiAudioEstaActivo();
        startActivity(new Intent(this, LoginActivity.class));
        finish();

    }


    public void comprobarSiAudioEstaActivo(){

        SharedPreferences sharedPref = getApplicationContext().getSharedPreferences("esPlay",1);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean("sonidoActivo",false);
        editor.apply();

        if (mediaPlayer != null){
            mediaPlayer.stop();
            mediaPlayer.release();
            mediaPlayer = null;
            if(holder==null){
                holder=(ListaCancionesAdapter.CancionViewHolder)adapter.getHolder();
            }

            holder.cambiarBoton();
        }

    }


    public void comprobarSiExisteVideo(final Cancion cancionElegida){

        ExistVideo video = new ExistVideo();
        video.setIdUsuario(identificadorUsuario);
        video.setIdCancion(cancionElegida.idCancion);

        showLoading();

        RequestInterface serviceExistVideo = ApiCliente.getService().create(RequestInterface.class);
        Call<ExistVideoResponse> existVideoResponse = serviceExistVideo.existVideo(video.getIdUsuario(),video.getIdCancion());

        existVideoResponse.enqueue(new Callback<ExistVideoResponse>() {
            @Override
            public void onResponse(Call<ExistVideoResponse> call, Response<ExistVideoResponse> response) {

                Log.i("FD", "Codigo Respuesta " +response.code());
                ExistVideoResponse existeVideoResponse = response.body();

                if (response.isSuccess()) {
                    hideLoading();
                    Log.i("FD","Código: "+existeVideoResponse.getVideo());

                    comprobarCodigoServidor(existeVideoResponse.getVideo(), cancionElegida);

                }else {
                    hideLoading();
                    Log.i("FD","Error: "+response.message());

                    mostrarAlerta(getString(R.string.error_conectar_servicio), true, cancionElegida);
                }
            }

            @Override
            public void onFailure(Call<ExistVideoResponse> call, Throwable t) {
                hideLoading();
                Log.i("FD","Error: " +t.getMessage());

                mostrarAlerta(getString(R.string.error_conectar_servicio), true, cancionElegida);
            }
        });

    }


    private void comprobarCodigoServidor(String codigo, Cancion cancionElegida){

        //boolean puedeSubirVideo = true;
        SharedPreferences sharedPref = getApplicationContext().getSharedPreferences("SubidaCancion",1);
        SharedPreferences.Editor editor = sharedPref.edit();

        switch (codigo){
            case ExistVideoResponse.EXIST_VIDEO_SI:

                editor.putBoolean("subirCancion",false);
                editor.putString("idCancion", cancionElegida.idCancion);
                editor.apply();


                //Mostrar Alerta de informacion de que ya ha subido un video de esa cancion
                mostrarAlerta(getString(R.string.errorExisteVideo), true, cancionElegida);

                break;

            case ExistVideoResponse.EXIST_VIDEO_NO:

                //Pasar pantalla de grabacion y puede subir video

                editor.putBoolean("subirCancion",true);
                editor.putString("idCancion", cancionElegida.idCancion);
                editor.apply();


                irAPantallaGrabacion(cancionElegida);

                break;

            case ExistVideoResponse.EXIST_VIDEO_NOTUSER:
                mostrarAlerta(getString(R.string.errorDesconocido), false, cancionElegida);
                break;

            case ExistVideoResponse.EXIST_VIDEO_NOTSONG:
                mostrarAlerta(getString(R.string.errorDesconocido), false, cancionElegida);
                break;

            default:
                mostrarAlerta(getString(R.string.errorDesconocido), false, cancionElegida);
        }
    }


    public void mostrarAlerta(String mensaje, final boolean tipoErrorAlerta, final Cancion cancion){

        String titulo = "";

        titulo = tipoErrorAlerta ? getString(R.string.error): getString(R.string.info);

        new AlertDialog.Builder(this)
                .setTitle(getString(R.string.info))
                .setMessage(mensaje)
                .setNeutralButton(android.R.string.ok,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                if (tipoErrorAlerta){
                                    dialog.cancel();
                                }else{
                                    irAPantallaGrabacion(cancion);
                                    dialog.cancel();
                                }


                            }
                        }
                )
                .setCancelable(false)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }


    public void irAPantallaGrabacion(final Cancion cancion){

        //final Cancion cancionSeleccionada = arrayCanciones.get(posicion);

        List<AppError> errorList = mainModel.checkAvailability();
        if (errorList.size() > 0) {
            String msg = "";
            for (AppError error : errorList) {
                msg += error.message + "\n";
            }
            new AlertDialog.Builder(this)
                    .setTitle(getString(R.string.headphones))
                    .setMessage(msg)
                    .setNeutralButton(android.R.string.ok,
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    goToActivity(identificadorUsuario, cancion);
                                }
                            }
                    )
                    .setCancelable(false)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        } else {

            goToActivity(identificadorUsuario, cancion);
        }

    }

    public void goToActivity(String idUser, Cancion cancion) {
        if(prefs==null){
            prefs=getSharedPreferences(getString(R.string.preferences), Context.MODE_PRIVATE);
        }
        prefs.edit().putLong("amount", 0).apply();
        Intent intent = new Intent(this, GrabacionVideoActivity.class);
        intent.putExtra("cancionSeleccionada",cancion);
        intent.putExtra("idUser",idUser);
        //intent.putExtra("puedeSubirVideo", subirVideo);
        startActivity(intent);
    }


    private void showLoading(){
        mProgressBar.setVisibility(View.VISIBLE);
    }

    private void hideLoading(){
        mProgressBar.setVisibility(View.GONE);
    }

    @Override
    protected void onResume()
    {
        deleteFiles();
        StatsManage.resumeActivity( this );
        super.onResume();
    }

    @Override
    protected void onPause()
    {
        prefs.edit().putBoolean("gotosing", true).apply();
        StatsManage.pauseActivity();
        super.onPause();
    }


}
