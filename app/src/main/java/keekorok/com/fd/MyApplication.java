package keekorok.com.fd;

import android.content.Context;
import android.support.multidex.MultiDex;

import com.rtve.stats.StatsLib;

/**
 * Created by mac-27 on 21/2/17.
 */

public class MyApplication extends StatsLib
{

    @Override
    protected void attachBaseContext( Context base )
    {
        super.attachBaseContext( base );
        // 0. Obigatoriamente instalamos MultiDex ya que StatsLiibs hereda de MultidexApp.
        MultiDex.install( this );
    }

}
