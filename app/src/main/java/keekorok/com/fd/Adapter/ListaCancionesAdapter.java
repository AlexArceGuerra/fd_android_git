package keekorok.com.fd.Adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import keekorok.com.fd.Common.GlobalConstants;
import keekorok.com.fd.Modelos.Cancion;
import keekorok.com.fd.R;

/**
 * Created by juanfranciscocordoba on 29/12/16.
 */

public class ListaCancionesAdapter extends RecyclerView.Adapter<ListaCancionesAdapter.CancionViewHolder> {

    private List<Cancion> items;
    private final CustomItemClickListener listener;
    public Context context;
    public RecyclerView.ViewHolder holder;

    public static class CancionViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private CustomItemClickListener listener;

        public ImageView caratula;
        public TextView nombreCancion;
        public TextView nombreArtista;
        public ImageButton botonCantar;
        public ImageButton botonReproducirAudio;
        private MediaPlayer mediaPlayer;
        public String urlAudio;
       // private boolean selected=true;
        private boolean playing=false;
        private String audioIniciado;
        private int itemId=0;
        private SharedPreferences prefs;
        private boolean primeraPulsacion;


        public CancionViewHolder(View v) {
            super(v);
            caratula      = (ImageView) v.findViewById(R.id.caratula_cancion);
            nombreCancion = (TextView) v.findViewById(R.id.nombre_cancion);
            nombreArtista = (TextView) v.findViewById(R.id.nombre_artisita);
            botonCantar   = (ImageButton)   v.findViewById(R.id.btnCantar);
            botonReproducirAudio = (ImageButton) v.findViewById(R.id.btnReproducirAudio);
            botonCantar.setOnClickListener(this);
            botonReproducirAudio.setOnClickListener(this);
            urlAudio = "";
        }



        @Override
        public void onClick(View view) {
            if(prefs==null){
                Context c=caratula.getContext();
                prefs=c.getSharedPreferences(c.getString(R.string.preferences), Context.MODE_PRIVATE);
            }
                primeraPulsacion=prefs.getBoolean("primerapulsacion", true);


            if (listener != null) {
                GlobalConstants.context = view.getContext();
                //Se comprueba si ya esta reproduciendo
                SharedPreferences sharedPref = GlobalConstants.context.getSharedPreferences("esPlay",1);
                Boolean estaReproduciendo = sharedPref.getBoolean("sonidoActivo",false);

                /*if (estaReproduciendo && !botonReproducirAudio.isSelected()){
                    return;
                }*/

                if(view.getId() == R.id.btnReproducirAudio){



                    //Log.i("David", "isSelected es "+selected+" al pulsar el botón de play");
                    //botonReproducirAudio.setSelected();
                    //if(botonReproducirAudio.isSelected()){

                    if(primeraPulsacion){
                        if(playing){
                            //selected=false;
                            botonReproducirAudio.setSelected(false);
                            //botonReproducirAudio.setSelected(selected);

                            //SharedPreferences sharedPref = view.getContext().getSharedPreferences("esPlay",1);
                            SharedPreferences.Editor editor = sharedPref.edit();
                            editor.putBoolean("sonidoActivo",false);
                            editor.apply();

                            botonReproducirAudio.setImageResource(R.drawable.circled_play_filled);
                            botonCantar.setEnabled(true);
                            botonCantar.setAlpha(1f);
                            pararAudio();

                        }else{
                            //selected=true;
                            botonReproducirAudio.setSelected(true);
                            //botonReproducirAudio.setSelected(selected);
                            SharedPreferences.Editor editor = sharedPref.edit();
                            editor.putBoolean("sonidoActivo",true);
                            editor.apply();

                            botonReproducirAudio.setImageResource(R.drawable.circled_pause);
                            botonCantar.setEnabled(false);
                            botonCantar.setAlpha(0.1f);
                            itemId=itemView.getId();
                            prefs.edit().putBoolean("primerapulsacion", false).apply();
                            iniciarAudio(urlAudio);

                        }
                    }else{
                        if(itemId==itemView.getId()){
                            if(playing){
                                //selected=false;
                                botonReproducirAudio.setSelected(false);
                                //botonReproducirAudio.setSelected(selected);

                                //SharedPreferences sharedPref = view.getContext().getSharedPreferences("esPlay",1);
                                SharedPreferences.Editor editor = sharedPref.edit();
                                editor.putBoolean("sonidoActivo",false);
                                editor.apply();

                                botonReproducirAudio.setImageResource(R.drawable.circled_play_filled);
                                botonCantar.setEnabled(true);
                                botonCantar.setAlpha(1f);


                                pararAudio();

                            }else{
                                //selected=true;
                                botonReproducirAudio.setSelected(true);
                                //botonReproducirAudio.setSelected(selected);
                                SharedPreferences.Editor editor = sharedPref.edit();
                                editor.putBoolean("sonidoActivo",true);
                                editor.apply();

                                botonReproducirAudio.setImageResource(R.drawable.circled_pause);
                                botonCantar.setEnabled(false);
                                botonCantar.setAlpha(0.1f);
                                itemId=itemView.getId();
                                primeraPulsacion=false;

                                iniciarAudio(urlAudio);

                            }
                        }

                    }






                }

                listener.onItemClick(view,getAdapterPosition(),mediaPlayer);

            }

        }



        public void iniciarAudio(String urlAudioMini){

            Log.i("David", "Iniciar audio llamado");
            if(!playing){
                String urlAudio = urlAudioMini;

                mediaPlayer = new MediaPlayer();
                mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);

                try {
                    mediaPlayer.setDataSource(urlAudio);
                } catch (IllegalArgumentException e) {
                    //Toast.makeText(getApplicationContext(), "You might not set the URI correctly!", Toast.LENGTH_LONG).show();
                } catch (SecurityException e) {
                    //Toast.makeText(getApplicationContext(), "You might not set the URI correctly!", Toast.LENGTH_LONG).show();
                } catch (IllegalStateException e) {
                    //Toast.makeText(getApplicationContext(), "You might not set the URI correctly!", Toast.LENGTH_LONG).show();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    mediaPlayer.prepare();
                } catch (IllegalStateException e) {
                    //Toast.makeText(getApplicationContext(), "You might not set the URI correctly!", Toast.LENGTH_LONG).show();
                } catch (IOException e) {
                    //Toast.makeText(getApplicationContext(), "You might not set the URI correctly!", Toast.LENGTH_LONG).show();
                }

                mediaPlayer.start();
                playing=true;
                //selected=false;
                prefs.edit().putBoolean("gotosing", false).apply();
                mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mediaPlayer) {

                        SharedPreferences sharedPref = GlobalConstants.context.getSharedPreferences("esPlay",1);
                        SharedPreferences.Editor editor = sharedPref.edit();
                        editor.putBoolean("sonidoActivo",false);
                        editor.apply();

                        botonReproducirAudio.setImageResource(R.drawable.circled_play_filled);
                        botonCantar.setEnabled(true);
                        botonReproducirAudio.setSelected(true);
                        botonCantar.setAlpha(1f);
                        pararAudio();

                    }
                });
            }
            //String url = "http://programmerguru.com/android-tutorial/wp-content/uploads/2013/04/hosannatelugu.mp3";






        }

        public void pararAudio(){

            prefs.edit().putBoolean("primerapulsacion", true).apply();
            playing=false;
            try{
                mediaPlayer.stop();
                //selected=false;
                botonCantar.setEnabled(true);
                botonCantar.setAlpha(1f);
                prefs.edit().putBoolean("gotosing", true).apply();
            }catch (IllegalStateException ise){
                Log.i("David", "ISE lanzado");
                //selected=false;
                botonReproducirAudio.setSelected(false);
                botonCantar.setEnabled(true);
                botonCantar.setAlpha(1f);
                prefs.edit().putBoolean("gotosing", true).apply();
            }

            //mediaPlayer.reset();
            //mediaPlayer.release();

        }

        public boolean isPlaying() {
            return playing;
        }

        public void cambiarBoton(){
            botonCantar.setEnabled(true);
            botonCantar.setAlpha(1f);
        }
    }

    public RecyclerView.ViewHolder getHolder() {
        return holder;
    }

    public ListaCancionesAdapter(Context context, final CustomItemClickListener listener) {

        items = new ArrayList<>();
        this.context = context;
        this.listener = listener;

    }

    @Override
    public int getItemCount() {

        return items.size();
    }

    public void addCancion(Cancion cancion) {
        Log.i("FD", String.format("%s - %s",cancion.autor,cancion.concurso));
        items.add(cancion);
        notifyDataSetChanged();
    }

    @Override
    public CancionViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_cancion, viewGroup, false);
        return new CancionViewHolder(v);
    }

    @Override
    public void onBindViewHolder(CancionViewHolder viewHolder, int i) {


        String urlCaratural = "http://"+items.get(i).getImagenMini();
        //viewHolder.caratula.setImageResource(items.get(i).getImagenMini());
        Glide.with(viewHolder.caratula.getContext())
                .load(urlCaratural)
                .placeholder(R.drawable.fantasticduo_v2)
                .centerCrop()
                .into(viewHolder.caratula);
        viewHolder.nombreCancion.setText(items.get(i).getTema());
        viewHolder.nombreArtista.setText(items.get(i).getAutor());

        viewHolder.urlAudio = "http://"+items.get(i).getUrlAudioMini();

        if(viewHolder.botonReproducirAudio.isSelected()){

            viewHolder.botonReproducirAudio.setImageResource(R.drawable.circled_pause);
            viewHolder.botonCantar.setEnabled(false);
            viewHolder.botonCantar.setAlpha(0.1f);


        }else{

            viewHolder.botonReproducirAudio.setImageResource(R.drawable.circled_play_filled);
            viewHolder.botonCantar.setEnabled(true);
            viewHolder.botonCantar.setAlpha(1f);

        }

        viewHolder.listener = this.listener;
        holder=viewHolder;
        //viewHolder.items = this.items;
    }





    /*
    public boolean isEnabled(Context ct,int position) {

        SharedPreferences sharedPref = ct.getSharedPreferences("celdaPulsada",1);
        int posicionSeleccionada = sharedPref.getInt("celda",1);

        if (posicionSeleccionada == -1 || posicionSeleccionada == position)
            return true;
        return false;
    }
    */

}

