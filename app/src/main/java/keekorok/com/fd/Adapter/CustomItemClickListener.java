package keekorok.com.fd.Adapter;

import android.media.MediaPlayer;
import android.view.View;


/**
 * Created by juanfranciscocordoba on 9/1/17.
 */

public interface CustomItemClickListener {

    void onItemClick(View v, int position, MediaPlayer reproductor);

}
