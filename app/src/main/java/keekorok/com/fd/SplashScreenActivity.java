package keekorok.com.fd;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.WindowManager;

import com.rtve.stats.StatsManage;

import java.util.Timer;
import java.util.TimerTask;

import keekorok.com.fd.Utilidades.FDConstants;
import keekorok.com.fd.Utilidades.StatsManagerHelper;

public class SplashScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        StatsManagerHelper.initStatsManager( getApplication(), getApplicationContext() );

        setContentView(R.layout.activity_splash);

        StatsManagerHelper.sendView(StatsManagerHelper.PANTALLA_Splash);

        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        activateTimer();
    }



    private void activateTimer() {
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                launchActivityWithoutTransition(LoginActivity.class);
            }
        };

        Timer timer = new Timer();
        timer.schedule(task, FDConstants.SPLASH_SCREEN_DELAY);
    }

    protected void launchActivityWithoutTransition(Class<?> cls) {
        Intent intent = new Intent(SplashScreenActivity.this, cls);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION
                | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        overridePendingTransition(0, 0);
        startActivity(intent);
        overridePendingTransition(0, 0);
        finish();
    }

    @Override
    protected void onResume()
    {
        StatsManage.resumeActivity( this );
        super.onResume();
    }

    @Override
    protected void onPause()
    {
        StatsManage.pauseActivity();
        super.onPause();
    }

    @Override
    protected void onDestroy()
    {
        StatsManage.appClose();
        super.onDestroy();
    }



}
