package keekorok.com.fd;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.rtve.stats.StatsManage;

import keekorok.com.fd.Common.GlobalConstants;
import keekorok.com.fd.Common.StorageUtils;
import keekorok.com.fd.Common.ValidationsUtil;
import keekorok.com.fd.Modelos.Alerta;
import keekorok.com.fd.Modelos.LoginResponse;
import keekorok.com.fd.Modelos.RequestInterface;
import keekorok.com.fd.Modelos.ResetPassword;
import keekorok.com.fd.Modelos.ResetPasswordResponse;
import keekorok.com.fd.RestService.ApiCliente;
import keekorok.com.fd.Utilidades.StatsManagerHelper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RecuperarPassActivity extends AppCompatActivity {

    private EditText mEmailEditText;
    private ProgressBar mProgressBar;
    private TextView textoDescripcion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recuperar_pass);
        mEmailEditText = (EditText) findViewById(R.id.texto_input_email);
        textoDescripcion = (TextView) findViewById(R.id.texto_descripcion);
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        StatsManagerHelper.initStatsManager(getApplication(), getApplicationContext());
        StatsManagerHelper.sendView(StatsManagerHelper.Pantalla_RecPass);
        configurarTipografia();
    }

    /*@Override
    public void onBackPressed()
    {
        return;
    }*/

    public void irAtras(View view){
        finish();
    }


    public void configurarTipografia(){

        Typeface typeface = Typeface.createFromAsset(getAssets(), "HelveticaNeueLight.ttf");

        mEmailEditText.setTypeface(typeface);
        textoDescripcion.setTypeface(typeface);
    }

    public void enviarPassword(View view){
        String textoEmail = mEmailEditText.getText().toString();
        if(ValidationsUtil.isValidEmail(textoEmail)){
            doResetPassword(textoEmail);

        }else{
            Toast.makeText(getApplicationContext(), getString(R.string.introduzcaEmailCorrecto), Toast.LENGTH_SHORT).show();
        }
    }


    private void doResetPassword(String email){
        ResetPassword resetPassword = new ResetPassword();
        resetPassword.setEmail(email);

        showLoading();
        RequestInterface serviceResetPassword= ApiCliente.getService().create(RequestInterface.class);
        Call<ResetPasswordResponse> resetPasswordResponse = serviceResetPassword.resetPassword(resetPassword);

        resetPasswordResponse.enqueue(new Callback<ResetPasswordResponse>() {
            @Override
            public void onResponse(Call<ResetPasswordResponse> call, Response<ResetPasswordResponse> response) {
                Log.i("FD", "Codigo Respuesta " +response.code());
                ResetPasswordResponse resetPasswordResponse = response.body();
                if (response.isSuccess()) {
                    hideLoading();
                    Log.i("FD","Código: "+resetPasswordResponse.getCodigoCtr());
                    comprobarCodigoServidor(resetPasswordResponse.getCodigoCtr());
                }else {
                    hideLoading();
                    Log.i("FD","Error: "+response.message());
                    mostrarAlerta(getString(R.string.error_conectar_servicio));
                }
            }

            @Override
            public void onFailure(Call<ResetPasswordResponse> call, Throwable t) {
                hideLoading();
                Log.i("FD","Error: " +t.getMessage());
                mostrarAlerta(getString(R.string.error_conectar_servicio));
            }
        });
    }


    private void comprobarCodigoServidor(String codigo){
        switch (codigo){
            case ResetPasswordResponse.RESET_PASS_OK:
                goToLogin();
                break;

            case ResetPasswordResponse.RESET_PASS_NOT_REGISTERED:
                mostrarAlerta(getString(R.string.errorResetNotRegister));
                break;

            case ResetPasswordResponse.RESET_PASS_KO:
                mostrarAlerta(getString(R.string.errorResetUserKO));
                break;

            default:
                mostrarAlerta(getString(R.string.errorDesconocido));
        }
    }

    private void goToLogin(){

        mostrarAlertaIrLogin(getString(R.string.resetPassOKTitle), getString(R.string.resetPassOKDescrip));

    }

    public void mostrarAlertaIrLogin(String titulo, String mensaje){

        new AlertDialog.Builder(this)
                .setTitle(titulo)
                .setMessage(mensaje)
                .setNeutralButton(android.R.string.ok,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                finish();
                            }
                        }
                )
                .setCancelable(false)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();


    }


    public void mostrarAlerta(String mensaje){

        new AlertDialog.Builder(this)
                .setTitle(getString(R.string.error))
                .setMessage(mensaje)
                .setNeutralButton(android.R.string.ok,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        }
                )
                .setCancelable(false)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }


    private void showLoading(){
        mProgressBar.setVisibility(View.VISIBLE);
    }

    private void hideLoading(){
        mProgressBar.setVisibility(View.GONE);
    }

    @Override
    protected void onResume()
    {
        StatsManage.resumeActivity( this );
        super.onResume();
    }

    @Override
    protected void onPause()
    {
        StatsManage.pauseActivity();
        super.onPause();
    }

    @Override
    protected void onDestroy()
    {
        StatsManage.appClose();
        super.onDestroy();
    }
}
