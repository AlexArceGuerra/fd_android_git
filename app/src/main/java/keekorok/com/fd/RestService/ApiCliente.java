package keekorok.com.fd.RestService;

import keekorok.com.fd.Modelos.RequestInterface;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by juanfranciscocordoba on 5/1/17.
 */

public class ApiCliente {
    //http://81.45.66.119:8080
    //public static final String BASE_URL = "http://34.250.208.108/fanduo/rest/";
    //public static final String BASE_URL = "http://34.250.232.250/fanduo/rest/";
    public static final String BASE_URL = "http://fanduo.info"+"/"+"fanduo/rest/";
    private static Retrofit retrofit = null;

    public static Retrofit getService() {

        if (retrofit==null) {

            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

        }

        return retrofit;

    }

}
