package keekorok.com.fd.Modelos;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by juanfranciscocordoba on 28/12/16.
 */

public interface RequestInterface {

    @GET("listacanciones")
    Call<ListaCancionesResponse> getListaCanciones(@Query("activo") String activo, @Query("device") String device);


    @POST("RegistroServ")
    Call<LoginResponse> getAcceso(@Body Login login);

    @POST("EmailServ")
    Call<ResetPasswordResponse> resetPassword(@Body ResetPassword resetPassword);

    @GET("ExistVideo")
    Call<ExistVideoResponse> existVideo(@Query("idUsuario") String idUsuario, @Query("idCancion") String idCancion);

    @POST("UploadVideo")
    Call<UploadVideoResponse> uploadVideo(@Body UploadVideo video);

}
