package keekorok.com.fd.Modelos;




import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;


/**
 * Created by juanfranciscocordoba on 18/1/17.
 */

public class Alerta  {

    public myOnClickListener myListener;
    public Context context;
    public String titulo;
    public String mensaje;
    public String textoBoton;



    public Alerta(Context context, myOnClickListener myclick, String titulo, String mensaje, String textoBoton) {
        this.context = context;
        this.myListener = myclick;
        this.titulo = titulo;
        this.mensaje = mensaje;
        this.textoBoton = textoBoton;

        crearAlerta(context,myListener,titulo,mensaje,textoBoton);

    }

    public interface myOnClickListener {
        void botonPulsado();
    }


    public void crearAlerta(Context context, myOnClickListener myclick, String titulo, String mensaje, String textoBoton){

        AlertDialog.Builder builder = new AlertDialog.Builder(
                context);
        builder.setTitle(titulo);
        builder.setMessage(mensaje);
        builder.setPositiveButton(textoBoton,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,
                                        int which) {
                        //myListener.botonPulsado();
                    }
                });

        builder.show();

    }

}
