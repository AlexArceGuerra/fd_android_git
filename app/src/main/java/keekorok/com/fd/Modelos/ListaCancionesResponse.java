package keekorok.com.fd.Modelos;

import java.util.List;

/**
 * Created by juanfranciscocordoba on 26/12/16.
 */

public class ListaCancionesResponse {

    public List<Cancion> canciones;

    public List<Cancion> getCanciones() {

        return canciones;

    }

}
