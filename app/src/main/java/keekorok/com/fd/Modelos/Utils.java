package keekorok.com.fd.Modelos;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import keekorok.com.fd.R;
import keekorok.com.fd.Utilidades.AppError;
import keekorok.com.fd.Utilidades.FDConstants;


/**
 * Created by juanfranciscocordoba on 8/2/17.
 */

public class Utils {

    private static Utils instance;
    public boolean initialized;
    public Context context;
    public Activity activity;
    public String rootPath;
    public boolean isConnected;
    public boolean isWiFi;
    public ProgressDialog busy;


    public static Utils getInstance() {
        if (instance == null) {
            instance = new Utils();
        }
        return instance;
    }

    private Utils() {
    }

    public void initialize(Context context, Activity activity) {
        if (!initialized) {
            initialized = true;
            this.context = context;
            this.activity = activity;

            initData();
        }
    }

    private void initData() {

        // Root path
        rootPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS).getAbsolutePath();
    }

    public File getFileDirectory(String folder) {
        File dir = new File(rootPath, folder);

        // Create the storage directory if it does not exist
        if (!dir.exists()) {
            if (!dir.mkdirs()) {
                dir = null;
            }
        }
        return dir;
    }

    public String getFilePath(String folder) {
        String result = "";
        File file = getFileDirectory(folder);
        if (file != null) {
            result = file.getAbsolutePath() + File.separator;
        }
        return result;
    }


    public void writeToFile (String path, String filename, String text) {
        File file = new File(path, filename);
        FileWriter fileWritter;
        try {
            fileWritter = new FileWriter(file, true);
            fileWritter.append(text + FDConstants.NEW_LINE_SEPARATOR);
            fileWritter.flush();
            fileWritter.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*
    public void showDebug(String msg) {
        if (isDebug) {
            String warning = context.getString(R.string.msg_debug_warning);
            Toast toast = Toast.makeText(context.getApplicationContext(), msg + "\n\n" + warning, Toast.LENGTH_SHORT);
            toast.show();
        }
    }
    */

    public void showInfo(String title, String msg) {
        new AlertDialog.Builder(activity)
                .setTitle(title)
                .setMessage(msg)
                .setNeutralButton(android.R.string.ok,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        }
                )
                .setIcon(android.R.drawable.ic_dialog_info)
                .setCancelable(false)
                .show();
    }


    public void showError (String title, String msg) {
        new AlertDialog.Builder(activity)
                .setTitle(title)
                .setMessage(msg)
                .setNeutralButton(android.R.string.ok,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        }
                )
                .setCancelable(false)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    public Bitmap getImage(String filepath) {
        Bitmap result = null;
        if (filepath != null && filepath != "") {
            FileInputStream in = null;
            try {
                in = new FileInputStream(filepath);
                result = BitmapFactory.decodeStream(in);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    public List<AppError> checkAvailability() {
        List<AppError> errorList = new ArrayList<AppError>();
        AppError error = null;

        /*error = checkConnection();
        if (error != null) {
            errorList.add(error);
        }*/

        // CAUTION: not check it in emulator device!!!
        if (!isEmulator()) {
            error = checkHeadphones();
            if (error != null) {
                errorList.add(error);
            }

        }

        error = checkMicrophone();
        if (error != null) {
            errorList.add(error);
        }

        /*error = checkServices();
        if (error != null) {
            errorList.add(error);
        }*/

        return errorList;
    }

    public AppError checkConnection() {
        AppError error = null;
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = connectivityManager.getActiveNetworkInfo();

        isConnected = false;
        isWiFi = false;
        if (activeNetwork != null) {
            isConnected = activeNetwork.isConnectedOrConnecting();
            isWiFi = activeNetwork.getType() == ConnectivityManager.TYPE_WIFI;
        }

        if (!isConnected) {
            error = new AppError();
            error.message = context.getString(R.string.error_connection);
            error.code = FDConstants.ERROR.CONNECTION.getValue();
        }

        return error;
    }

    public AppError checkHeadphones() {
        AppError error = null;

        AudioManager am = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        boolean headphonesPresent = am.isWiredHeadsetOn();
        if (!headphonesPresent) {
            error = new AppError();
            error.message = context.getString(R.string.error_headphones);
            error.code = FDConstants.ERROR.HEADHPHONES.getValue();
        }

        return error;
    }

    public AppError checkMicrophone() {
        AppError error = null;

        PackageManager pm = context.getPackageManager();
        boolean microphonePresent = pm.hasSystemFeature(PackageManager.FEATURE_MICROPHONE);

        if (!microphonePresent) {
            error = new AppError();
            error.message = context.getString(R.string.error_microphone);
            error.code = FDConstants.ERROR.MICROPHONE.getValue();
        }

        return error;
    }

    public AppError  checkServices() {
        // TODO: checkServices
        AppError error = null;

        /*error = new AppError();
        error.message = context.getString(R.string.error_services);
        error.code = FDConstants.ERROR.SERVICES.getValue();*/

        return error;
    }

    public void updateVolume(int percentage) {
        Resources res = context.getResources();
        AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        Configuration conf = res.getConfiguration();
        DisplayMetrics dm = res.getDisplayMetrics();


        audioManager.setStreamVolume(AudioManager.STREAM_ALARM,
                getVolumeFromPercentage(audioManager.getStreamMaxVolume(AudioManager.STREAM_ALARM), percentage), AudioManager.FLAG_PLAY_SOUND);
        audioManager.setStreamVolume(AudioManager.STREAM_SYSTEM,
                getVolumeFromPercentage(audioManager.getStreamMaxVolume(AudioManager.STREAM_SYSTEM), percentage), AudioManager.FLAG_PLAY_SOUND);
        audioManager.setStreamVolume(AudioManager.STREAM_RING,
                getVolumeFromPercentage(audioManager.getStreamMaxVolume(AudioManager.STREAM_RING), percentage), AudioManager.FLAG_PLAY_SOUND);
        audioManager.setStreamVolume(AudioManager.STREAM_DTMF,
                getVolumeFromPercentage(audioManager.getStreamMaxVolume(AudioManager.STREAM_DTMF), percentage), AudioManager.FLAG_PLAY_SOUND);
        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC,
                getVolumeFromPercentage(audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC), percentage), AudioManager.FLAG_PLAY_SOUND);
        audioManager.setStreamVolume(AudioManager.STREAM_NOTIFICATION,
                getVolumeFromPercentage(audioManager.getStreamMaxVolume(AudioManager.STREAM_NOTIFICATION), percentage), AudioManager.FLAG_PLAY_SOUND);
        audioManager.setStreamVolume(AudioManager.STREAM_VOICE_CALL,
                getVolumeFromPercentage(audioManager.getStreamMaxVolume(AudioManager.STREAM_VOICE_CALL), percentage), AudioManager.FLAG_PLAY_SOUND);

        res.updateConfiguration(conf, dm);
    }

    private int getVolumeFromPercentage(int maxVolumeStream, int percentage) {
        return (percentage * maxVolumeStream) / 100;
    }

    public static boolean isEmulator() {
        return Build.FINGERPRINT.startsWith("generic")
                || Build.FINGERPRINT.startsWith("unknown")
                || Build.MODEL.contains("google_sdk")
                || Build.MODEL.contains("Emulator")
                || Build.MODEL.contains("Android SDK built for x86")
                || Build.MANUFACTURER.contains("Genymotion")
                || (Build.BRAND.startsWith("generic") && Build.DEVICE.startsWith("generic"))
                || "google_sdk".equals(Build.PRODUCT);
    }

    public static String getDateString(Date date, String pattern, Locale locale) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(pattern, locale);
            String result = sdf.format(date);

            return result;
        } catch (Exception e) {
            return "";
        }
    }


    public void showProgress(Context ctx, boolean isCancelable) {
        //Log.i(TAG, "showProgress()");
        busy = new ProgressDialog(ctx);
        busy.show();
        busy.setCancelable(isCancelable);
        busy.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        busy.setContentView(R.layout.loader);
        busy.setCancelable(false);
    }

    public void hideProgress() {
        if (busy != null) {
            //Log.i(TAG, "hideProgress()");
            busy.dismiss();
        }
    }

}
