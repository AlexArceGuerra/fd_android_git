package keekorok.com.fd.Modelos;

/**
 * Created by juanfranciscocordoba on 13/2/17.
 */

public class ExistVideoResponse {

    public static final String EXIST_VIDEO_SI = "SI";
    public static final String EXIST_VIDEO_NO = "NO";
    public static final String EXIST_VIDEO_NOTSONG = "NOTSONG";
    public static final String EXIST_VIDEO_NOTUSER = "NOTUSER";

    private String video;

    public String getVideo() {
        return video;
    }

}
