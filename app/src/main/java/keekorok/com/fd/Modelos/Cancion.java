package keekorok.com.fd.Modelos;

import java.io.Serializable;

/**
 * Created by juanfranciscocordoba on 26/12/16.
 */

public class Cancion implements Serializable {

    public String idCancion;
    public String activo;
    public String concurso;
    public String imagenMini;
    public String urlVideoBase;
    public String urlVideoTexto;
    public String urlAudioMini;
    public String autor;
    public String tema;
    public String var1;
    public String var2;
    public String var3;
    private boolean descargarTexto;

    public Cancion() {
    }

    public Cancion(String idCancion, String activo, String concurso, String imagenMini, String urlVideoBase, String urlVideoTexto, String urlAudioMini, String autor, String tema, String var1, String var2, String var3) {
        this.idCancion = idCancion;
        this.activo = activo;
        this.concurso = concurso;
        this.imagenMini = imagenMini;
        this.urlVideoBase = urlVideoBase;
        this.urlVideoTexto = urlVideoTexto;
        this.urlAudioMini = urlAudioMini;
        this.autor = autor;
        this.tema = tema;
        this.var1 = var1;
        this.var2 = var2;
        this.var3 = var3;
    }

    public String getIdCancion() {

        return idCancion;
    }

    public void setIdCancion(String idCancion) {

        this.idCancion = idCancion;
    }

    public String getActivo() {

        return activo;
    }

    public void setActivo(String activo) {

        this.activo = activo;
    }

    public String getConcurso() {

        return concurso;
    }

    public void setConcurso(String concurso) {

        this.concurso = concurso;
    }

    public String getImagenMini() {

        return imagenMini;
    }

    public void setImagenMini(String imagenMini) {

        this.imagenMini = imagenMini;
    }

    public String getUrlVideoBase() {

        return urlVideoBase;
    }

    public void setUrlVideoBase(String urlVideoBase) {

        this.urlVideoBase = urlVideoBase;
    }

    public String getUrlVideoTexto() {

        return urlVideoTexto;
    }

    public void setUrlVideoTexto(String urlVideoTexto) {

        this.urlVideoTexto = urlVideoTexto;
    }

    public String getUrlAudioMini() {

        return urlAudioMini;
    }

    public void setUrlAudioMini(String urlAudioMini) {

        this.urlAudioMini = urlAudioMini;
    }

    public String getAutor() {

        return autor;
    }

    public void setAutor(String autor) {

        this.autor = autor;
    }

    public String getTema() {

        return tema;
    }

    public void setTema(String tema) {

        this.tema = tema;
    }

    public String getVar1() {

        return var1;
    }

    public void setVar1(String var1) {

        this.var1 = var1;
    }

    public String getVar2() {

        return var2;
    }

    public void setVar2(String var2) {

        this.var2 = var2;
    }

    public String getVar3() {

        return var3;
    }

    public void setVar3(String var3) {

        this.var3 = var3;

    }

    public boolean isDescargarTexto() {
        return descargarTexto;
    }

    public void setDescargarTexto(boolean descargarTexto) {
        this.descargarTexto = descargarTexto;
    }
}


