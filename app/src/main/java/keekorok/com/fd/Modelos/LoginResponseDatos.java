package keekorok.com.fd.Modelos;

import java.io.Serializable;

/**
 * Created by juanfranciscocordoba on 11/1/17.
 */

public class LoginResponseDatos implements Serializable {

    private String codigoCtr;
    private String idUsuario;
    private String email;
    private String pass;
    private String telefono;
    private String nombre;
    private String apellidos;
    private String edad;
    private String ciudad;

    public String getCodigoCtr() {
        return codigoCtr;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public String getEmail() {
        return email;
    }

    public String getPass() {
        return pass;
    }

    public String getTelefono() {
        return telefono;
    }

    public String getNombre() {
        return nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public String getEdad() {
        return edad;
    }

    public String getCiudad() {
        return ciudad;
    }
}
