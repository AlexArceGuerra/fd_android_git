package keekorok.com.fd.Modelos;

/**
 * Created by juanfranciscocordoba on 15/2/17.
 */

public class UploadVideoResponse {

    public static final String UPLOAD_VIDEO_OK = "OK";
    public static final String UPLOAD_VIDEO_DUPLICATED = "DUPLICATED";
    public static final String UPLOAD_VIDEO_NOTSONG = "NOTSONG";
    public static final String UPLOAD_VIDEO_NOTUSER = "NOTUSER";

    private String video;

    public String getVideo() {
        return video;

    }
}
