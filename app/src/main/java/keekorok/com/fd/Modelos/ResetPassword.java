package keekorok.com.fd.Modelos;

/**
 * Created by joseignaciosanzgarcia1 on 11/2/17.
 */

public class ResetPassword {

    private String email;
    private String idUsuario;

    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }
}
