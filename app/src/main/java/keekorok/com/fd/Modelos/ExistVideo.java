package keekorok.com.fd.Modelos;

/**
 * Created by juanfranciscocordoba on 13/2/17.
 */

public class ExistVideo {

    private String idUsuario;
    private String idCancion;

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getIdCancion() {
        return idCancion;
    }

    public void setIdCancion(String idCancion) {
        this.idCancion = idCancion;
    }
}
