package keekorok.com.fd.Modelos;

/**
 * Created by juanfranciscocordoba on 15/2/17.
 */

public class UploadVideo {

    private String idUsuario;
    private String idCancion;
    private String urlCancionOrig;
    private String urlCancionUser;
    private String estado1;

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getIdCancion() {
        return idCancion;
    }

    public void setIdCancion(String idCancion) {
        this.idCancion = idCancion;
    }

    public String getUrlCancionOrig() {
        return urlCancionOrig;
    }

    public void setUrlCancionOrig(String urlCancionOrig) {
        this.urlCancionOrig = urlCancionOrig;
    }

    public String getUrlCancionUser() {
        return urlCancionUser;
    }

    public void setUrlCancionUser(String urlCancionUser) {
        this.urlCancionUser = urlCancionUser;
    }

    public String getEstado1() {
        return estado1;
    }

    public void setEstado1(String estado1) {
        this.estado1 = estado1;
    }
}
