package keekorok.com.fd.Modelos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by juanfranciscocordoba on 10/1/17.
 */

public class LoginResponse implements Serializable {

    public static final String REGISTRO_OK = "Added";
    public static final String USUARIO_REGISTRADO = "Registered";
    public static final String CONTRASENIA_NO_VALIDA = "InvalidPass";
    public static final String NO_REGISTRADO ="NotRegister";
    public static final String NECESITA_REGISTRO ="NeedRegister";

    @SerializedName("usuario")
    @Expose
    public LoginResponseDatos usuario;


    /*
    @SerializedName("codigoCtr")
    @Expose
    private String codigoCtr;

    @SerializedName("idUsuario")
    @Expose
    private String idUsuario;

    @SerializedName("email")
    @Expose
    private String email;

    @SerializedName("pass")
    @Expose
    private String pass;

    @SerializedName("telefono")
    @Expose
    private String telefono;

    @SerializedName("nombre")
    @Expose
    private String nombre;

    @SerializedName("apellidos")
    @Expose
    private String apellidos;

    @SerializedName("edad")
    @Expose
    private String edad;

    @SerializedName("ciudad")
    @Expose
    private String ciudad;


    public String getCodigoCtr() {

        return codigoCtr;
    }

    public String getIdUsuario() {

        return idUsuario;
    }

    public String getEmail() {

        return email;
    }

    public String getPass() {

        return pass;
    }

    public String getTelefono() {

        return telefono;
    }

    public String getNombre() {

        return nombre;
    }

    public String getApellidos() {

        return apellidos;
    }

    public String getEdad() {
        return edad;
    }

    public String getCiudad() {
        return ciudad;
    }

    */

}
