package keekorok.com.fd.Modelos;

/**
 * Created by joseignaciosanzgarcia1 on 11/2/17.
 */

public class ResetPasswordResponse {

    public static final String RESET_PASS_OK = "OK";
    public static final String RESET_PASS_NOT_REGISTERED = "NOTREGISTER";
    public static final String RESET_PASS_KO = "ERROR";

    private String codigoCtr;
    public String getCodigoCtr() {
        return codigoCtr;
    }
}
