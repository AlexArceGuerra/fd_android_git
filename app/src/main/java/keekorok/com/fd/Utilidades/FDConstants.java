package keekorok.com.fd.Utilidades;

/**
 * Created by juanfranciscocordoba on 8/2/17.
 */

public class FDConstants {

    public static final long SPLASH_SCREEN_DELAY = 2000;
    public static final long VIDEO_DIFFERENCE_MSG = 1000;
    //public static final String VIDEO_FILE = "video.mp4";

    public enum ERROR {
        CONNECTION(1), HEADHPHONES(2), MICROPHONE(3), SERVICES(4);
        public final int value;
        ERROR(final int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

    public enum APP_FOLDER {
        FD ("FD"), // General directory of the application data
        Temp ("FD/temp"); // resources temp directory

        private final String text;
        APP_FOLDER(final String text) {
            this.text = text;
        }
        @Override
        public String toString() {
            return text;
        }
    }

    public static final String NEW_LINE_SEPARATOR = "\n";
    public static final char COLUMN_SEPARATOR = ';';
    public static final String VIDEO_SUFFIX = ".mp4";
    public static final String VIDEO_FILE = "video" + VIDEO_SUFFIX;

}
