package keekorok.com.fd.Utilidades;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.VideoView;

/**
 * Created by juanfranciscocordoba on 8/2/17.
 */

public class CustomVideoView extends VideoView {

    protected int _overrideWidth = 480;

    protected int _overrideHeight = 360;

    public CustomVideoView(Context context) {
        super(context);
    }

    public CustomVideoView(Context context, AttributeSet set) {
        super(context, set);
    }

    public void resizeVideo(int width, int height) {
        _overrideHeight = height;
        _overrideWidth = width;
        // not sure whether it is useful or not but safe to do so
        getHolder().setFixedSize(width, height);
        //getHolder().setSizeFromLayout();
        requestLayout();
        invalidate(); // very important, so that onMeasure will be triggered
    }


    public void resizeToFill(int width, int height) {
        float relation = 0;

        if (width > height) {
            if (height>0) {
                relation = Float.valueOf(_overrideWidth)/Float.valueOf(_overrideHeight);
            }
            _overrideWidth = width;
            _overrideHeight = (int) (width/relation);
        } else {
            if (width>0) {
                relation = Float.valueOf(_overrideHeight) / Float.valueOf(_overrideWidth);
            }
            _overrideHeight = height;
            _overrideWidth = (int) (height/relation);
        }

        // not sure whether it is useful or not but safe to do so
        getHolder().setFixedSize(_overrideWidth, _overrideHeight);
        requestLayout();
        invalidate(); // very important, so that onMeasure will be triggered
    }


    public void forceResizeToFill(int width, int height, boolean forceHeight) {
        float relation = 0;
        if (forceHeight) {
            if (height>0) {
                relation = Float.valueOf(_overrideWidth)/Float.valueOf(_overrideHeight);
            }
            _overrideHeight = height;
            _overrideWidth = (int) (height * relation);
        } else {
            if (width>0) {
                relation = Float.valueOf(_overrideHeight) / Float.valueOf(_overrideWidth);
            }
            _overrideWidth = width;
            _overrideHeight = (int) (width * relation);
        }

        // not sure whether it is useful or not but safe to do so
        getHolder().setFixedSize(_overrideWidth, _overrideHeight);
        requestLayout();
        invalidate(); // very important, so that onMeasure will be triggered
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
    {
        setMeasuredDimension(_overrideWidth, _overrideHeight);
    }


}
