package keekorok.com.fd.Utilidades;

/**
 * Created by juanfranciscocordoba on 8/2/17.
 */

public class AppError {

    public int code;
    public String message;

    public AppError() {
    }

    @Override
    public String toString() {
        return "Code: " + code + ", Error: " + message;
    }


}
