package keekorok.com.fd.Utilidades;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import com.rtve.stats.GoogleAnalyticsSessionManager;
import com.rtve.stats.StatsLib;
import com.rtve.stats.StatsManage;
import com.rtve.stats.doraemon.DoraemonStatsService;

import android.app.Application;
import android.content.Context;
import android.support.annotation.StringDef;

import keekorok.com.fd.R;

// Helper para las estadisticas.
public class StatsManagerHelper
{

    // Pantalla a enviar de las estadisticas.
    public static final String PANTALLA_Splash = "SplashScreen";
    public static final String Pantalla_Login = "Login";
    public static final String Pantalla_Lista = "Lista de Canciones";
    public static final String Pantalla_Registro = "Registro de usuario";
    public static final String Pantalla_RecPass = "Recuperar contraseña";
    public static final String Pantalla_Grabacion = "Grabación";
    public static final String Pantalla_Visualizar = "Reproducción video grabado";
    public static final String Pantalla_Condiciones = "Leer condiciones";


    // Data de las pantallas a enviar.
    private static final String DORAEMON_BASE_STRING = "RTVE:APP:%s_PruebaEstadisticas";

    // Configuracion de las estadisticas.
    private static boolean IsAdobeEnabled = true;
    private static boolean IsComScoreEnabled = true;
    private static boolean IsGoogleEnabled = true;
    private static boolean AreEventsEnabled = true;
    private static boolean AreViewsEnabled = true;
    private static boolean AreStreamTagsEnabled = true;
    private static boolean Configuration[] = new boolean[]
            {
                    IsAdobeEnabled, IsComScoreEnabled, IsGoogleEnabled, AreEventsEnabled, AreViewsEnabled, AreStreamTagsEnabled
            };

    /**
     * Inicializa el StatsManager.
     */
    public static void initStatsManager( Application application, Context context )
    {
        String appName = application.getResources().getString( R.string.app_name );

        GoogleAnalyticsSessionManager.setTracker( ( ( StatsLib ) context ).getTracker( StatsLib.TrackerName.APP_TRACKER ) );
        StatsManage.inicApplication( application, appName, "CATEGORY", Configuration );
        DoraemonStatsService.newInstance( context );
    }

    /**
     * Envía un cambio de pantalla a Doraemon.
     */
    public static void sendView( @DoraemonViews String screenName )
    {
        String screenParam = String.format( DORAEMON_BASE_STRING, screenName );
        StatsManage.sendView( screenParam );
    }

    @Retention ( RetentionPolicy.SOURCE )
    @StringDef (
            {
                    PANTALLA_Splash,
                    Pantalla_Login,
                    Pantalla_Lista,
                    Pantalla_Registro,
                    Pantalla_RecPass,
                    Pantalla_Grabacion,
                    Pantalla_Visualizar,
                    Pantalla_Condiciones

            } )
    @interface DoraemonViews
    {
    }
}
