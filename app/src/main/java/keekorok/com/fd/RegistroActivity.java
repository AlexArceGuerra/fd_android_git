package keekorok.com.fd;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.rtve.stats.StatsManage;

import keekorok.com.fd.Common.FormatUtils;
import keekorok.com.fd.Common.StorageUtils;
import keekorok.com.fd.Common.ValidationsUtil;
import keekorok.com.fd.Modelos.Alerta;
import keekorok.com.fd.Modelos.Login;
import keekorok.com.fd.Modelos.LoginResponse;
import keekorok.com.fd.Modelos.LoginResponseDatos;
import keekorok.com.fd.Modelos.RequestInterface;
import keekorok.com.fd.RestService.ApiCliente;
import keekorok.com.fd.Utilidades.StatsManagerHelper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegistroActivity extends AppCompatActivity {

    private int REQUEST_CODE = 0;

    private EditText mEmailTextView;
    private EditText mPassTextView;
    private EditText mRepeatPassTextView;
    private EditText mPhoneTextView;
    private EditText mFirstNameTextView;
    private EditText mLastNameTextView;
    private EditText mCityTextView;
    private EditText mAgeTextView;

    private ImageButton mBtnCheckCG;
    private Button mBtnCCGG;

    private ProgressBar mProgressBar;

    private boolean acceptConditions = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);
        mEmailTextView = (EditText) findViewById(R.id.texto_input_email);
        mPassTextView = (EditText) findViewById(R.id.texto_input_pass);
        mRepeatPassTextView = (EditText) findViewById(R.id.texto_input_pass_confirm);
        mPhoneTextView = (EditText) findViewById(R.id.texto_input_telefono);
        mFirstNameTextView = (EditText) findViewById(R.id.texto_input_nombre);
        mLastNameTextView = (EditText) findViewById(R.id.texto_input_apellidos);
        mCityTextView = (EditText) findViewById(R.id.texto_input_ciudad);
        mAgeTextView = (EditText) findViewById(R.id.texto_input_edad);
        mBtnCheckCG = (ImageButton) findViewById(R.id.btnCheckCG);
        mBtnCCGG = (Button) findViewById(R.id.btnCCGG);
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        StatsManagerHelper.initStatsManager(getApplication(), getApplicationContext());
        StatsManagerHelper.sendView(StatsManagerHelper.Pantalla_Registro);
        mBtnCheckCG.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(RegistroActivity.this,getString(R.string.readagreement), Toast.LENGTH_LONG).show();
            }
        });
        configurarTipografia();
    }


    public void configurarTipografia(){

        Typeface typeface = Typeface.createFromAsset(getAssets(), "HelveticaNeueLight.ttf");

        mEmailTextView.setTypeface(typeface);
        mPassTextView.setTypeface(typeface);
        mRepeatPassTextView.setTypeface(typeface);
        mPhoneTextView.setTypeface(typeface);
        mFirstNameTextView.setTypeface(typeface);
        mLastNameTextView.setTypeface(typeface);
        mCityTextView.setTypeface(typeface);
        mBtnCCGG.setTypeface(typeface);

    }

    public void irAtras(View view){
        finish();
    }

    public void doRegistro(View view){
        String email = mEmailTextView.getText().toString();
        String pass = mPassTextView.getText().toString();
        String phone = mPhoneTextView.getText().toString();
        phone = FormatUtils.formatPhone(phone);
        String firstName = mFirstNameTextView.getText().toString();
        String lastName = mLastNameTextView.getText().toString();
        String city = mCityTextView.getText().toString();
        String age = mAgeTextView.getText().toString();

        if(checkValidateAndShowToast()){
            Login login = new Login();
            login.setEmail(email);
            login.setPass(pass);
            login.setTelefono(phone);
            login.setNombre(firstName);
            login.setApellidos(lastName);
            login.setCiudad(city);
            login.setEdad(age);
            login.setTipoAccion("Registro");
            callToRegistro(login);
        }
    }



    private void callToRegistro(Login login){
        showLoading();
        RequestInterface serviceRegistro = ApiCliente.getService().create(RequestInterface.class);
        Call<LoginResponse> loginResponse = serviceRegistro.getAcceso(login);

        loginResponse.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                Log.i("FD", "Codigo Respuesta " +response.code());
                LoginResponse loginResponse = response.body();
                if (response.isSuccess()) {
                    hideLoading();
                    Log.i("FD","Código: "+loginResponse.usuario.getCodigoCtr());
                    comprobarCodigoServidor(loginResponse.usuario);
                }else {
                    hideLoading();
                    Log.i("FD","Error: "+response.message());
                    mostrarAlerta(getString(R.string.error_conectar_servicio));
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                hideLoading();
                Log.i("FD","Error: " +t.getMessage());
                mostrarAlerta(getString(R.string.error_conectar_servicio));
            }
        });

    }


    private void comprobarCodigoServidor(LoginResponseDatos loginResponseDatos){
        String codigoCtr = loginResponseDatos.getCodigoCtr();
        switch (codigoCtr){
            case LoginResponse.REGISTRO_OK:
                String email = loginResponseDatos.getEmail();
                String idUsuario = loginResponseDatos.getIdUsuario();
                goToListaCanciones(email, idUsuario);
                break;

            case LoginResponse.USUARIO_REGISTRADO:
                mostrarAlerta(getString(R.string.errorRegisterUserExists));
                break;

            case LoginResponse.CONTRASENIA_NO_VALIDA:
                mostrarAlerta(getString(R.string.errorRegisterContrasena));
                break;

            case LoginResponse.NO_REGISTRADO:
            case LoginResponse.NECESITA_REGISTRO:
                mostrarAlerta(getString(R.string.errorRegisterNotCompleted));
                break;

            default:
                mostrarAlerta(getString(R.string.errorDesconocido));
        }
    }

    private void goToListaCanciones(String email, String idUsuario){
        if(email != null && idUsuario!=null) {
            StorageUtils.putPreferenceValue(this, email, idUsuario, StorageUtils.PreferenceValueType.STRING);
        }

        mostrarAlertaIrListaCanciones(getString(R.string.registroOKTitle), getString(R.string.registroOKDescrip));

    }


    /**
     * Comprueba si existen errores de formato en el formulario
     * @param -
     * @return: Booleano. Devuelve true si el formulario es correcto.
     **/
    private boolean checkValidateAndShowToast(){

        String email = mEmailTextView.getText().toString();
        String pass = mPassTextView.getText().toString();
        String repeatPass = mRepeatPassTextView.getText().toString();
        String phone = mPhoneTextView.getText().toString();
        phone = FormatUtils.formatPhone(phone);
        String firstName = mFirstNameTextView.getText().toString();
        String lastName = mLastNameTextView.getText().toString();
        String city = mCityTextView.getText().toString();
        String age = mAgeTextView.getText().toString();

        if(!ValidationsUtil.isValidEmail(email)){
            String error = getString(R.string.introduzcaEmailCorrecto);
            showToast(error);
            return false;
        }

        if(!ValidationsUtil.isValidPass(pass)){
            String error = getString(R.string.passNoValida);
            showToast(error);
            return false;
        }

        if(!TextUtils.equals(pass, repeatPass)){
            String error = getString(R.string.passNoCoinciden);
            showToast(error);
            return false;
        }

        /*if(!ValidationsUtil.isValidPhone(phone)){
            String error = getString(R.string.telefonoNoValido);
            showToast(error);
            return false;
        }*/

        /*if(TextUtils.isEmpty(firstName)){
            String error = getString(R.string.nombreIncorreto);
            showToast(error);
            return false;
        }

        if(TextUtils.isEmpty(lastName)){
            String error = getString(R.string.apellidosIncorreto);
            showToast(error);
            return false;
        }

        if(TextUtils.isEmpty(city)){
            String error = getString(R.string.ciudadIncorreta);
            showToast(error);
            return false;
        }

        if(!ValidationsUtil.isValidAge(age)){
            String error = getString(R.string.edadNoValidad);
            showToast(error);
            return false;
        }*/

        if(!acceptConditions){
            String error = getString(R.string.debeAceptarCondiciones);
            showToast(error);
            return false;
        }

        return true;
    }


    private void showToast(String error){
        Toast.makeText(this, error, Toast.LENGTH_LONG).show();
    }

    private void showLoading(){
        mProgressBar.setVisibility(View.VISIBLE);
    }

    private void hideLoading(){
        mProgressBar.setVisibility(View.GONE);
    }


    public void mostrarAlertaIrListaCanciones(String titulo, String mensaje){

        new AlertDialog.Builder(this)
                .setTitle(titulo)
                .setMessage(mensaje)
                .setNeutralButton(android.R.string.ok,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                Intent intent = new Intent(RegistroActivity.this, ListaCanciones.class);
                                startActivity(intent);
                            }
                        }
                )
                .setCancelable(false)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();


    }

    public void mostrarAlerta(String mensaje){

        new AlertDialog.Builder(this)
                .setTitle(getString(R.string.error))
                .setMessage(mensaje)
                .setNeutralButton(android.R.string.ok,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        }
                )
                .setCancelable(false)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    public void leerCondiciones(View view) {
        Intent intent = new Intent(this, LeerCondicionesActivity.class);
        startActivityForResult(intent, REQUEST_CODE);
    }


    private void processChoiceCCGG(boolean choice) {
        acceptConditions = choice;
        if (choice) {
            mBtnCheckCG.setImageResource(R.drawable.checked_checkbo_filled);
            mBtnCCGG.setText(getString(R.string.acceptedCCGG));
        } else {
            mBtnCheckCG.setImageResource(R.drawable.unchecked_checkbox);
            mBtnCCGG.setText(getString(R.string.deniedCCGG));
        }

        int grayColor = ContextCompat.getColor(this, R.color.gris_claro);
        mBtnCCGG.setTextColor(grayColor);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            boolean choice = data.getBooleanExtra(LeerCondicionesActivity.EXTRA_CONDITIONS, false);
            processChoiceCCGG(choice);
        }
    }

    /*@Override
    public void onBackPressed()
    {
        return;
    }*/

    @Override
    protected void onResume()
    {
        StatsManage.resumeActivity( this );
        super.onResume();
    }

    @Override
    protected void onPause()
    {
        StatsManage.pauseActivity();
        super.onPause();
    }

    @Override
    protected void onDestroy()
    {
        StatsManage.appClose();
        super.onDestroy();
    }
}
