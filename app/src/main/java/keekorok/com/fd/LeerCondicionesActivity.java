package keekorok.com.fd;

import android.animation.Animator;
import android.app.Activity;
import android.content.Intent;
import android.graphics.PointF;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;

import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.listener.OnLoadCompleteListener;
import com.github.barteksc.pdfviewer.listener.OnPageChangeListener;
import com.github.barteksc.pdfviewer.scroll.DefaultScrollHandle;
import com.rtve.stats.StatsManage;

import keekorok.com.fd.Utilidades.StatsManagerHelper;

public class LeerCondicionesActivity extends AppCompatActivity  implements OnPageChangeListener, OnLoadCompleteListener {
    private static final String TAG= LeerCondicionesActivity.class.getSimpleName();

    private String filename = "Condiciones_Uso_App_180117.pdf";
    public static String EXTRA_CONDITIONS;
    private PDFView mPdfView;
    private LinearLayout mContainerButtons;
    Integer pageNumber = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leer_condiciones);

        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        mPdfView = (PDFView) findViewById(R.id.pdfView);
        mContainerButtons = (LinearLayout) findViewById(R.id.container_buttons);
        StatsManagerHelper.initStatsManager(getApplication(), getApplicationContext());
        StatsManagerHelper.sendView(StatsManagerHelper.Pantalla_Condiciones);
        displayFromAsset(filename);
    }

    @Override
    public void loadComplete(int nbPages) {
        mPdfView.zoomCenteredTo(2.4f, new PointF());
    }

    @Override
    public void onPageChanged(int page, int pageCount) {
        if(page == (pageCount-1)) {
            // move in
            mContainerButtons.animate().translationY(-20f).setDuration(800).start();
        }else {
            // move away
            mContainerButtons.animate().translationY(250f).setDuration(800).start();
        }
    }


    private void displayFromAsset(String assetFileName) {
        filename = assetFileName;
        mPdfView.fromAsset(filename)
                .defaultPage(pageNumber)
                .onPageChange(this)
                .enableAnnotationRendering(true)
                .onLoad(this)
                .scrollHandle(new DefaultScrollHandle(this))
                .load();
    }


    public void choiceConditions(boolean userChoice){
        Intent resultIntent = new Intent();
        resultIntent.putExtra(EXTRA_CONDITIONS, userChoice);
        setResult(Activity.RESULT_OK, resultIntent);
        finish();
    }

    public void accept(View view){
        choiceConditions(true);
    }

    public void deny(View view){
        choiceConditions(false);
    }

    @Override
    protected void onResume()
    {
        StatsManage.resumeActivity( this );
        super.onResume();
    }

    @Override
    protected void onPause()
    {
        StatsManage.pauseActivity();
        super.onPause();
    }

    @Override
    protected void onDestroy()
    {
        StatsManage.appClose();
        super.onDestroy();
    }
}
