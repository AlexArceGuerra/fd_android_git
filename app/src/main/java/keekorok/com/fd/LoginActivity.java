package keekorok.com.fd;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.rtve.stats.StatsManage;

import keekorok.com.fd.Common.StorageUtils;
import keekorok.com.fd.Modelos.Alerta;
import keekorok.com.fd.Modelos.Login;
import keekorok.com.fd.Modelos.LoginResponse;
import keekorok.com.fd.Modelos.LoginResponseDatos;
import keekorok.com.fd.Modelos.RequestInterface;
import keekorok.com.fd.Modelos.Utils;
import keekorok.com.fd.RestService.ApiCliente;
import keekorok.com.fd.Utilidades.NetworkUtil;
import keekorok.com.fd.Utilidades.StatsManagerHelper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener{

    public static final String USUARIO_REGISTRADO = "Registered";
    public static final String CONTRASENIA_NO_VALIDA = "InvalidPass";
    public static final String NO_REGISTRADO ="NotRegister";
    public static final String NECESITA_REGISTRO ="NeedRegister";

    public ImageButton botonEntrar;
    public Button botonRegistro;
    public EditText textoEmail;
    public EditText textoPass;
    public Button botonRecordarPass;
    private ProgressBar spinner;
    public Utils mainModel = Utils.getInstance();

    public Alerta.myOnClickListener myListenerAlerta;
    private SharedPreferences prefs;
    private TextView versionTv;
    String versionName = BuildConfig.VERSION_NAME;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        prefs=getSharedPreferences(getString(R.string.preferences), Context.MODE_PRIVATE);
        prefs.edit().putBoolean("primerapulsacion", true).apply();
        prefs.edit().putBoolean("gotosing", true).apply();
        versionTv=(TextView)findViewById(R.id.versiontv);
        versionTv.setText("Versión: "+versionName);
        relacionarObjetosVista();
        configurarTipografia();
        fijarEventosBotones();
        StatsManagerHelper.initStatsManager(getApplication(), getApplicationContext());
        StatsManagerHelper.sendView(StatsManagerHelper.Pantalla_Login);

        fijarListenerChangedTextoEmail();
        fijarListenerChangedTextoPass();

    }

    //*********************** Listener Botones ****************************
    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.btnEntrar:
                comprobarDatosAccesoCorrectos();
                break;

            case R.id.btnRegistro:
                irAPantallaRegistro();
                break;

            case R.id.btnRecuperarPass:
                irRecuperarPass();
                break;

            default:
                break;
        }

    }

    //*********************** Listener EditText ****************************
    public void fijarListenerChangedTextoEmail(){

        textoEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


    }

    public void fijarListenerChangedTextoPass(){

        textoPass.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int count) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


    }

    //*********************** Metodos ****************************
    public void relacionarObjetosVista() {

        textoEmail = (EditText) findViewById(R.id.texto_input_email);
        textoPass = (EditText) findViewById(R.id.texto_input_pass);
        botonEntrar = (ImageButton) findViewById(R.id.btnEntrar);
        botonRegistro = (Button) findViewById(R.id.btnRegistro);
        botonRecordarPass = (Button) findViewById(R.id.btnRecuperarPass);
        spinner=(ProgressBar)findViewById(R.id.progressBar);
        spinner.getIndeterminateDrawable().setColorFilter(Color.parseColor("#FF00FF"), android.graphics.PorterDuff.Mode.SRC_ATOP);
        spinner.setVisibility(View.GONE);
        if(!prefs.getString("email", "").equals("")){
            textoEmail.setText(prefs.getString("email", ""));
        }
        if(!prefs.getString("pass", "").equals("")){
            textoPass.setText(prefs.getString("pass", ""));
        }

    }

    public void configurarTipografia(){

        Typeface typeface = Typeface.createFromAsset(getAssets(), "HelveticaNeueLight.ttf");

        botonRegistro.setTypeface(typeface);
        botonRecordarPass.setTypeface(typeface);
        textoEmail.setTypeface(typeface);
        textoPass.setTypeface(typeface);

    }

    public void fijarEventosBotones(){

        botonEntrar.setOnClickListener(this);
        botonRegistro.setOnClickListener(this);
        botonRecordarPass.setOnClickListener(this);

    }

    public void comprobarDatosAccesoCorrectos(){

        CharSequence temp_emilID=textoEmail.getText().toString();
        if(!isValidEmail(temp_emilID))
        {
            textoEmail.requestFocus();
            Toast.makeText(getApplicationContext(), "Introduzca una dirección de correo correcta", Toast.LENGTH_SHORT).show();

        }
        else
        {

            if (textoPass.length() > 0){

                //Enviar credenciales al servicio.
                spinner.setVisibility(View.VISIBLE);
                realizarLogin();

            }

        }



    }

    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public boolean isValid(String s) {
        String n = ".*[0-9].*";
        String a = ".*[A-Z].*";
        return s.matches(n) && s.matches(a);
    }


    //*********************** Conectar Servicio ****************************
    public void realizarLogin (){


        //Se comprueba si existe conexion
        int estadoConexion = NetworkUtil.getConnectivityStatus(getApplicationContext());

        if (estadoConexion == 0) {

            ocultarLoading();
            mostrarAlerta(getString(R.string.error_connection));

        }else{

            Login login = new Login();
            login.setEmail(textoEmail.getText().toString());
            login.setPass(textoPass.getText().toString());
            login.setTipoAccion("Login");

            RequestInterface serviceLogin = ApiCliente.getService().create(RequestInterface.class);
            Call<LoginResponse> loginResponse = serviceLogin.getAcceso(login);

            loginResponse.enqueue(new Callback<LoginResponse>() {
                @Override
                public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {

                    Log.i("FD", "Codigo Respuesta " +response.code());

                    if (!response.isSuccess()) {
                        ocultarLoading();
                        Log.i("FD","Error: "+response.message());
                        mostrarAlerta(getString(R.string.error_conectar_servicio));

                    }else {
                        ocultarLoading();
                        LoginResponse loginResponse = response.body();

                        Log.i("FD","Código: "+loginResponse.usuario.getCodigoCtr());
                        comprobarCodigoServidor(loginResponse.usuario);

                    }

                }

                @Override
                public void onFailure(Call<LoginResponse> call, Throwable t) {
                    ocultarLoading();
                    Log.i("FD","Error: " +t.getMessage());
                    mostrarAlerta(getString(R.string.error_conectar_servicio));
                }
            });


        }


    }

    public void comprobarCodigoServidor(LoginResponseDatos loginResponseDatos){
        String codigoCtr = loginResponseDatos.getCodigoCtr();
        switch (codigoCtr){

            case USUARIO_REGISTRADO:
                String idUsuario = loginResponseDatos.getIdUsuario();
                pasarAListado(idUsuario);
                prefs.edit().putString("email", textoEmail.getText().toString().trim()).apply();
                prefs.edit().putString("pass", textoPass.getText().toString().trim()).apply();
                break;
            case CONTRASENIA_NO_VALIDA:
                mostrarAlerta(getString(R.string.error_pass_incorrecta));
                break;
            case NO_REGISTRADO:
                mostrarAlerta(getString(R.string.error_usuario_debe_registrarse));
                break;
            case NECESITA_REGISTRO:
                mostrarAlerta(getString(R.string.error_usuario_debe_registrarse));
                break;

        }

    }

    public void mostrarAlerta(String mensaje){

        new AlertDialog.Builder(this)
                .setTitle(getString(R.string.error))
                .setMessage(mensaje)
                .setNeutralButton(android.R.string.ok,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        }
                )
                .setCancelable(false)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    public void pasarAListado (String idUsuario){

        Intent intent = new Intent(this, ListaCanciones.class);
        intent.putExtra("idUsuario", idUsuario);
        startActivity(intent);

    }

    public void irAPantallaRegistro (){

        Intent intentRegistro = new Intent(this, RegistroActivity.class);
        startActivity(intentRegistro);

    }

    public void irRecuperarPass(){
        Intent intent = new Intent(this, RecuperarPassActivity.class);
        startActivity(intent);
    }

    public void ocultarLoading (){
        spinner.setVisibility(View.GONE);
    }

    //Para habilitar/deshabilitar los eventos
    public static void setViewGroupEnabled(ViewGroup view, boolean enabled)
    {
        int childern = view.getChildCount();

        for (int i = 0; i< childern ; i++)
        {
            View child = view.getChildAt(i);
            if (child instanceof ViewGroup)
            {
                setViewGroupEnabled((ViewGroup) child, enabled);
            }
            child.setEnabled(enabled);
        }
        view.setEnabled(enabled);
    }

    @Override
    public void onBackPressed()
    {

        SharedPreferences sharedPref = getApplicationContext().getSharedPreferences("esPlay",1);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean("sonidoActivo",false);
        editor.apply();

        moveTaskToBack(true);
        android.os.Process.killProcess(android.os.Process.myPid());
        System.exit(1);
    }

    @Override
    protected void onResume()
    {
        StatsManage.resumeActivity( this );
        super.onResume();
    }

    @Override
    protected void onPause()
    {
        StatsManage.pauseActivity();
        super.onPause();
    }

    @Override
    protected void onDestroy()
    {
        StatsManage.appClose();
        super.onDestroy();
    }

}
