package keekorok.com.fd.Common;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by joseignaciosanzgarcia1 on 11/2/17.
 */

public class StorageUtils {

    private static final String TAG = StorageUtils.class.getSimpleName();

    public enum PreferenceValueType {
        INT,
        FLOAT,
        LONG,
        BOOLEAN,
        STRING
    }

    public static void putPreferenceValue(final Context context, final String key, final Object value, final PreferenceValueType type) {
        final SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        final SharedPreferences.Editor editor = settings.edit();

        putPreferenceValueInEditor(context, editor, key, value, type);
        editor.commit();
    }


    public static void putPreferenceValueInEditor(final Context context, final SharedPreferences.Editor editor, final String key, Object value, final PreferenceValueType type) {
        PreferenceValueType finalType = type;

        switch (finalType) {
            case BOOLEAN:
                editor.putBoolean(key, (Boolean) value);
                break;
            case FLOAT:
                editor.putFloat(key, (Float) value);
                break;
            case INT:
                editor.putInt(key, (Integer) value);
                break;
            case LONG:
                editor.putLong(key, (Long) value);
                break;
            case STRING:
                editor.putString(key, (String) value);
                break;
            default:
                break;
        }

    }

    public static void deletePreferenceValue(final Context context, final String key) {
        final SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        final SharedPreferences.Editor editor = settings.edit();
        editor.remove(key);
        editor.commit();
    }


    public static Object getPreferenceValue(final Context context, final String key, final PreferenceValueType type) {
        final SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        Object value = null;

        if (settings.contains(key)) {
            switch (type) {
                case BOOLEAN:
                    value = settings.getBoolean(key, false);
                    break;
                case FLOAT:
                    value = settings.getFloat(key, 0);
                    break;
                case INT:
                    value = settings.getInt(key, 0);
                    break;
                case LONG:
                    value = settings.getLong(key, 0);
                    break;
                case STRING:
                    value = settings.getString(key, null);
                    break;
                default:
                    break;
            }
        }

        return value;
    }

    public static void deleteAll(final Context context, String nameFileKeyStore) {
        final SharedPreferences pref = context.getSharedPreferences(nameFileKeyStore, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.clear();
        editor.commit();
    }


    public static void deletePrefereDataValue(final Context context, String key, String nameFileKeyStore) {
        final SharedPreferences pref = context.getSharedPreferences(nameFileKeyStore, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.remove(key);
        editor.commit();
    }

}
