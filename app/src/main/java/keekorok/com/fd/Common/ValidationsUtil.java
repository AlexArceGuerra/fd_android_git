package keekorok.com.fd.Common;

import android.text.TextUtils;
import android.util.Patterns;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by joseignaciosanzgarcia1 on 11/2/17.
 */

public final class ValidationsUtil {

    private static int PASS_SIZE = 8;
    private static int PHONE_SIZE = 9;
    private static int AGE_SIZE = 1;


    public static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    /*public static boolean isValidPhone(CharSequence target){
        return !TextUtils.isEmpty(target) && target.length() == PHONE_SIZE  &&  TextUtils.isDigitsOnly(target);
    }*/


    public static boolean isValidAge(CharSequence target){
        return !TextUtils.isEmpty(target) && target.length() >= AGE_SIZE  &&  TextUtils.isDigitsOnly(target);
    }


    public static  boolean isValidPass(CharSequence target) {
        String text = target.toString();
        Pattern patternOneNumber = Pattern.compile(".*[0-9]+.*");
        Matcher matcherOneNumber = patternOneNumber.matcher(text);

        Pattern patternOneCharacter = Pattern.compile(".*[a-zA-Z]+.*");
        Matcher matcherOneCharacter = patternOneCharacter.matcher(text);

        return !TextUtils.isEmpty(target) && target.length() >= PASS_SIZE  && matcherOneCharacter.matches() && matcherOneNumber.matches();
    }
}
