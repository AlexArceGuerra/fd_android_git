package keekorok.com.fd.Common;

/**
 * Created by joseignaciosanzgarcia1 on 11/2/17.
 */

public final class FormatUtils {

    public static String formatPhone(String phone){
        phone = phone.replaceAll("\\s+","");
        phone = phone.replaceAll("\\-","");
        phone = phone.replaceAll("\\+","");
        return phone;
    }
}
