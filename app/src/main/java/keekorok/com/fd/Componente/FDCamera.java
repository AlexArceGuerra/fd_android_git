package keekorok.com.fd.Componente;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.hardware.Camera;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.io.File;
import java.io.IOException;
import java.util.List;

import keekorok.com.fd.Modelos.Utils;
import keekorok.com.fd.R;
import keekorok.com.fd.Utilidades.CameraResponse;
import keekorok.com.fd.Utilidades.FDConstants;

/**
 * Created by juanfranciscocordoba on 8/2/17.
 */

public class FDCamera extends SurfaceView implements SurfaceHolder.Callback {


    private Utils mainModel = Utils.getInstance();
    private Camera camera;
    private MediaRecorder mediaRecorder;
    private SurfaceHolder holder;
    public boolean isPreview, isRecording;
    private CameraResponse cameraResponse;
    private boolean isFrontCamera = true;
    private boolean camaraParada=false;
    private int veces=1;
    private Context ctx;
    private SharedPreferences prefs;
    public FDCamera(Context context) {
        super(context);
        holder = getHolder();
        holder.addCallback(this);
        // deprecated setting, but required on Android versions prior to 3.0
        holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        ctx=context;
    }


    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        //Log.d(TAG, "surfaceChanged");
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        //Log.d(TAG, "surfaceCreated");
        Log.i("David", "surfaceCreated llamado");
        setCamera();
        cameraResponse.onSurfaceCreated();
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        //Log.d(TAG, "surfaceDestroyed");
        releaseCamera();
    }

    public void registerCameraResponse(CameraResponse cameraResponse) {
        this.cameraResponse = cameraResponse;
    }

    private void stopCamera() {
        if (camera != null) {
            //camera.stopPreview();
            isPreview = false;
        }
    }


    // Stop isPreview and release camera
    private void releaseCamera() {
        if (camera != null) {
            stopCamera();
            camera.release();
            camera = null;
        }
    }

    private void setCamera() {
        try {
            releaseCamera();
            if (isFrontCamera) {
                camera = getFrontCamera();
                isFrontCamera = true;
                if(prefs==null){
                    prefs=ctx.getSharedPreferences(ctx.getString(R.string.preferences), Context.MODE_PRIVATE);
                }
                prefs.edit().putBoolean("isfrontcamera", true).apply();
            } else {
                camera = Camera.open();
                isFrontCamera = false;
                if(prefs==null){
                    prefs=ctx.getSharedPreferences(ctx.getString(R.string.preferences), Context.MODE_PRIVATE);
                }
                prefs.edit().putBoolean("isfrontcamera", false).apply();
            }

            if (camera != null) {
                Camera.Parameters cameraParameters = camera.getParameters();

                List<Camera.Size> sizes = cameraParameters.getSupportedPreviewSizes();
                Camera.Size optimalSize = getOptimalPreviewSize(sizes, getResources().getDisplayMetrics().widthPixels, getResources().getDisplayMetrics().heightPixels);
                cameraParameters.setPreviewSize(optimalSize.width, optimalSize.height);

                camera.setDisplayOrientation(180);

                //TODO: cameraParameters.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
                camera.setParameters(cameraParameters);

                Log.i("David", "antes de llamar a setPreviewDisplay");
                camera.setPreviewDisplay(holder);
                Log.i("David", "Después de llamar a setPreviewDisplay");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void startCamera() {
        if (camera != null) {
            Log.i("David", "Antes de startPreview, en FDCamera, método startCamera");
            try{
                camera.startPreview();
            }catch (Exception e){
                AlertDialog.Builder falloDialog=new AlertDialog.Builder(ctx);
                falloDialog.setTitle(ctx.getString(R.string.error));
                falloDialog.setMessage(ctx.getString(R.string.fallocamara));
                falloDialog.setPositiveButton(ctx.getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                falloDialog.show();
            }

            Log.i("David", "Después de startPreview, en FDCamera, método startCamera");
            isPreview = true;
        }
    }

    // Get front camera
    private Camera getFrontCamera() {
        int count = 0;
        Camera cam = null;
        Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
        count = Camera.getNumberOfCameras();
        for (int i = 0; i < count; i++) {
            Camera.getCameraInfo(i, cameraInfo);
            if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                try {
                    cam = Camera.open(i);
                } catch (RuntimeException e) {
                    //Log.e(TAG, "Camera failed to open: " + e.getLocalizedMessage());
                    e.printStackTrace();
                }
                break;
            }
        }
        // If no front camera, use default
        if (cam == null) {
            cam = Camera.open();
        }

        return cam;
    }

    public void stopMediaRecorder(){
        mediaRecorder.stop();
    }

    private boolean prepareVideoRecorder() {
        boolean result = false;
        if (camera != null) {
            if(mediaRecorder==null){
                mediaRecorder = new MediaRecorder();
            }


            // Step 1: Unlock and set camera to MediaRecorder
            try{
                camera.unlock();
            }catch (Exception e){
                Log.i("David", "Excepción: ");
                e.printStackTrace();
            }
            mediaRecorder.setCamera(camera);

            // Step 2: Set sources
            mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
            mediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);

            // Step 3: Set a CamcorderProfile (requires API Level 8 or higher)
            try{
                mediaRecorder.setProfile(CamcorderProfile.get(CamcorderProfile.QUALITY_QVGA));
                //mediaRecorder.setProfile(CamcorderProfile.get(CamcorderProfile.QUALITY_480P));
                //mediaRecorder.setProfile(CamcorderProfile.get(CamcorderProfile.QUALITY_LOW));
            }catch (Exception e){
                e.printStackTrace();
                mediaRecorder.setProfile(CamcorderProfile.get(CamcorderProfile.QUALITY_LOW));
            }

            mediaRecorder.setMaxDuration(0);

            // Step 4: Set output file
            String videoPath = mainModel.getFilePath(FDConstants.APP_FOLDER.FD.toString()) + FDConstants.VIDEO_FILE;
            File file=new File(videoPath);
            if(file.exists()){
                file.delete();
            }
            mediaRecorder.setOutputFile(videoPath);
            mediaRecorder.setOrientationHint(180);

            // Step 5: Set the isPreview output
            mediaRecorder.setPreviewDisplay(holder.getSurface());

            Log.i("David", "Después de setPreviewDisplay");
            // Step 6: Prepare configured MediaRecorder
            try {
                mediaRecorder.prepare();
                result = true;
            } catch (IllegalStateException e) {
                Log.e("David", "IllegalStateException preparing MediaRecorder: " + e.getMessage());
                releaseMediaRecorder();
            } catch (IOException e) {
                Log.e("David", "IOException preparing MediaRecorder: " + e.getMessage());
                releaseMediaRecorder();
            }
        }
        Log.i("David", "Devolvemos result: "+result);
        return result;
    }


    private void releaseMediaRecorder(){
        Log.i("David", "Método releaseMediaRecorder.");
        if (mediaRecorder != null) {
            //mediaRecorder.reset();   // clear recorder configuration
            //mediaRecorder.release(); // release the recorder object
            try {
                mediaRecorder.stop();
            }catch (Exception e){
                e.printStackTrace();
            }

            camera.stopPreview();
            mediaRecorder = null;
            camera.lock();           // lock camera for later use
        }
        isRecording = false;
        camaraParada=true;
    }

    public void startRecordVideo() {
        Log.i("David", "Esta es la vez nº "+veces+" que entramos en el método startRecordVideo");
        //startCamera();
        if (!isRecording && prepareVideoRecorder()) {
            try{
                mediaRecorder.start();
            }catch (Exception e){
                Log.i("David", "Pete. Entramos en el bucle");
                mediaRecorder=null;
                startRecordVideo();
            }

            isRecording = true;
            //mainModel.showDebug(mainModel.context.getString(R.string.msg_start_recording));
        } else {
            //releaseMediaRecorder();
            Log.i("David", "A ver qué pasa aquí");
        }
    }

    public void stopRecordVideo() {
        // CAUTION: MediaRecorder can't pause/resume recording!!!
        stopCamera();
        releaseMediaRecorder();
        //mainModel.showDebug(mainModel.context.getString(R.string.msg_stop_recording));
    }

    private Camera.Size getOptimalPreviewSize(List<Camera.Size> sizes, int w, int h) {
        final double ASPECT_TOLERANCE = 0.05;
        double targetRatio = (double) w/h;

        if (sizes==null) return null;

        Camera.Size optimalSize = null;

        double minDiff = Double.MAX_VALUE;

        int targetHeight = h;

        // Find size
        for (Camera.Size size : sizes) {
            double ratio = (double) size.width / size.height;
            if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE) continue;
            if (Math.abs(size.height - targetHeight) < minDiff) {
                optimalSize = size;
                minDiff = Math.abs(size.height - targetHeight);
            }
        }

        if (optimalSize == null) {
            minDiff = Double.MAX_VALUE;
            for (Camera.Size size : sizes) {
                if (Math.abs(size.height - targetHeight) < minDiff) {
                    optimalSize = size;
                    minDiff = Math.abs(size.height - targetHeight);
                }
            }
        }
        return optimalSize;
    }

    public Camera.Size getPreviewSize() {
        Camera.Size result = null;
        if (camera != null) {
            result = camera.getParameters().getPreviewSize();
        }
        return result;
    }

    public void changeCamera() {
        isFrontCamera = !isFrontCamera;
        setCamera();
    }

    public boolean isCamaraParada() {
        return camaraParada;
    }

    public void setCamaraParada(boolean camaraParada) {
        this.camaraParada = camaraParada;
    }

    public boolean isFrontCamera() {
        return isFrontCamera;
    }

    public void setFrontCamera(boolean frontCamera) {
        isFrontCamera = frontCamera;
    }
}
