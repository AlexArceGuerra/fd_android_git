package keekorok.com.fd;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Camera;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.rtve.stats.StatsManage;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import keekorok.com.fd.Componente.FDCamera;
import keekorok.com.fd.Modelos.Cancion;
import keekorok.com.fd.Modelos.RequestInterface;
import keekorok.com.fd.Modelos.UploadVideo;
import keekorok.com.fd.Modelos.UploadVideoResponse;
import keekorok.com.fd.Modelos.Utils;
import keekorok.com.fd.RestService.ApiCliente;
import keekorok.com.fd.Utilidades.AmazonS3Util;
import keekorok.com.fd.Utilidades.CameraResponse;
import keekorok.com.fd.Utilidades.CustomVideoView;
import keekorok.com.fd.Utilidades.FDConstants;
import keekorok.com.fd.Utilidades.StatsManagerHelper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GrabacionVideoActivity extends AppCompatActivity implements CameraResponse{

    CustomVideoView videoBase, videoText;
    FrameLayout videoBaseLayout, videoResultLayout/*, videoTextLayout*/;
    ImageView btRecord, btStop, btGoPlay, btCamera, btBack;
    FDCamera fdCamera;
    boolean isVideoTextReady, isVideoBaseReady;
    Cancion cancionElegida;
    String idUsuario;
    private ProgressDialog pDialog;
    private SharedPreferences prefs;
    private TransferUtility transferUtility;
    private File fileArtist;
    private File folder;
    private File fileText;
    private String videoId;
    private String videoPath;
    private android.app.AlertDialog.Builder aDialog;
    private boolean alertMostrado=false;
    private File file;

    public Utils mainModel = Utils.getInstance();
    public static final String VIDEO_BASE_URL =
            "http://81.45.66.119:6543/vod/mp4:laura_pausini_se_fue.mp4/playlist.m3u8";
    public static final String VIDEO_TEXT_URL =
            "http://81.45.66.119:6543/vod/mp4:laura_pausini_se_fue_texto.mp4/playlist.m3u8";

    private boolean cancionSubida=false;
    private boolean isDownloading=false;
    private boolean videoPreparado=false;
    private boolean videoTextPreparado=false;
    private int downloadNumber=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i("David", "onCreate");
        super.onCreate(savedInstanceState);

        //setContentView(R.layout.activity_video_record);
        setContentView(R.layout.newrecordayout);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        StatsManagerHelper.initStatsManager(getApplication(), getApplicationContext());
        StatsManagerHelper.sendView(StatsManagerHelper.Pantalla_Grabacion);
        Intent intent = getIntent();
        cancionElegida = (Cancion) intent.getSerializableExtra("cancionSeleccionada");
        if(prefs==null){
            prefs=getSharedPreferences(getString(R.string.preferences), Context.MODE_PRIVATE);
            prefs.edit().putBoolean("streamfinalizado", false).apply();
        }

        idUsuario = (String) intent.getSerializableExtra("idUser");
        pDialog=new ProgressDialog(GrabacionVideoActivity.this);
        pDialog.setTitle(getString(R.string.wait));
        pDialog.setMessage(getString(R.string.loadingvideo));
        pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        pDialog.setIndeterminate(false);
        pDialog.setProgress(0);
        pDialog.setMax(100);
        pDialog.setCancelable(false);
        pDialog.show();

        folder=new File(Environment.getExternalStorageDirectory().toString() + "/.FDVIDEOS/");
        fileArtist=new File(folder,".videopruebaartista.mp4");
        if(cancionElegida.isDescargarTexto()){
            fileText=new File(folder, ".videoText.mp4");
        }
        if(!fileArtist.exists() && !isDownloading && downloadNumber==0){
            isDownloading=true;
            downloadArtist();
        }else{
            pDialog.dismiss();
            /*if(!alertMostrado){
                if(aDialog==null){
                    aDialog=new android.app.AlertDialog.Builder(GrabacionVideoActivity.this);
                }
                aDialog.setTitle(getString(R.string.aviso));
                aDialog.setMessage(getString(R.string.artistnotshowing));
                aDialog.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                Log.i("David", "Mostramos el diálogo de la línea 154");
                alertMostrado=true;
                aDialog.show();
            }*/

        }






        iniciarCamara();
        videoResultLayout = (FrameLayout) findViewById(R.id.videoResultLayout);
        videoBaseLayout = (FrameLayout) findViewById(R.id.videoBaseLayout);
        videoResultLayout.addView(fdCamera);

        btGoPlay = (ImageView) findViewById(R.id.btUpload);

        btGoPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToPlay(view);
            }
        });

        btRecord = (ImageView) findViewById(R.id.btRecord);
        btRecord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("David", "Botón de grabar pulsado");
                downloadNumber=1;
                if(isVideoBaseReady/* && isVideoTextReady*/){

                    synchronizedRecord(true);
                }else{
                    //Mostrar diálogo de que los vídeos no están listos
                    Log.i("David", "En el else del btRecord. Alguno de los dos booleanos está a false");
                }

            }
        });
        btStop = (ImageView) findViewById(R.id.btStop);
        btStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("David", "Botón de parar pulsado");
                android.app.AlertDialog.Builder stopDialog=new android.app.AlertDialog.Builder(GrabacionVideoActivity.this);
                stopDialog.setTitle(getString(R.string.aviso));
                stopDialog.setMessage(getString(R.string.avisotext));
                stopDialog.setPositiveButton(getString(R.string.si), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(GrabacionVideoActivity.this,getString(R.string.stoppingvideo), Toast.LENGTH_LONG).show();
                        Thread thread=new Thread(new Runnable() {
                            @Override
                            public void run() {

                                stopVideos();
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        btStop.setVisibility(View.GONE);
                                        btBack.setVisibility(View.GONE);
                                        btCamera.setVisibility(View.GONE);
                                    }
                                });

                                if(prefs==null){
                                    prefs=getSharedPreferences(getString(R.string.preferences), Context.MODE_PRIVATE);

                                }
                                prefs.edit().putBoolean("streamfinalizado", false).apply();
                            }
                        });
                        thread.start();
                        dialog.dismiss();
                    }
                });
                stopDialog.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                stopDialog.show();
            }
        });

        btCamera = (ImageView) findViewById(R.id.btCamera);
        btCamera.setVisibility(View.VISIBLE);
        btBack = (ImageView) findViewById(R.id.btBack);
        btBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goBack(view);
            }
        });
        videoBase = (CustomVideoView) findViewById(R.id.videoBase);
        videoText=(CustomVideoView)findViewById(R.id.videotext);
        if(!videoPreparado){
            prepararVideoBase();
        }

        if(!videoTextPreparado){
            prepararVideoText();
        }

        // Initialize
        initVideos();

    }

    private void prepararVideoText() {
        videoTextPreparado=true;
        videoText.setOnInfoListener(new MediaPlayer.OnInfoListener() {
            @Override
            public boolean onInfo(MediaPlayer mp, int what, int extra) {
                if(what==MediaPlayer.MEDIA_INFO_VIDEO_RENDERING_START){
                    return true;
                }
                return false;
            }
        });

        videoText.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                Log.i("David", "onPreparedListener del videoText llamado");
                isVideoTextReady = true;
                if(pDialog.isShowing()){
                    pDialog.dismiss();
                    /*if(!alertMostrado){
                        if(aDialog==null){
                            aDialog=new android.app.AlertDialog.Builder(GrabacionVideoActivity.this);
                        }
                        aDialog.setTitle(getString(R.string.aviso));
                        aDialog.setMessage(getString(R.string.artistnotshowing));
                        aDialog.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                alertMostrado=true;
                            }
                        });
                        Log.i("David", "Mostramos el diálogo de la línea 436");
                        aDialog.show();
                    }*/

                }

                videoText.seekTo(0);

                if (!videoTextPreparado) {
                    showControls();
                }
                videoText.setOnPreparedListener(null);
            }
        });

        videoText.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                //stopRecording();
                Log.i("David", "videobase onErrorListener");
                mainModel.showError(getString(R.string.error),getString(R.string.error_video_base));
                stopRecording();
                stopVideos();
                return true;
            }
        });
    }

    private void prepararVideoBase() {
        videoPreparado=true;
        videoBase.setOnInfoListener(new MediaPlayer.OnInfoListener() {
            @Override
            public boolean onInfo(MediaPlayer mp, int what, int extra) {
                if(what==MediaPlayer.MEDIA_INFO_VIDEO_RENDERING_START){

                    return true;
                }
                return false;
            }
        });

        videoBase.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {

                Log.i("David", "onPreparedListener del videoBase llamado");
                isVideoBaseReady = true;
                if(pDialog.isShowing()){
                    pDialog.dismiss();
                    /*if(!alertMostrado){
                        if(aDialog==null){
                            aDialog=new android.app.AlertDialog.Builder(GrabacionVideoActivity.this);
                        }
                        aDialog.setTitle(getString(R.string.aviso));
                        aDialog.setMessage(getString(R.string.artistnotshowing));
                        aDialog.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                alertMostrado=true;
                            }
                        });
                        Log.i("David", "Mostramos el diálogo de la línea 317");
                        aDialog.show();
                    }*/

                }

                videoBase.seekTo(0);
                Log.i("DavidAlejandro", "Valores que le vamos a poner: ");
                Log.i("DavidAlejandro", "Width: "+videoBaseLayout.getWidth());
                Log.i("DavidAlejandro", "Height: "+videoBaseLayout.getHeight());
                Log.i("DavidAlejandro", "---------");
                int width=videoBaseLayout.getWidth()-15;
                int height=videoBaseLayout.getHeight()-15;
                ((CustomVideoView)videoBase).forceResizeToFill(videoBaseLayout.getWidth(), videoBaseLayout.getHeight(), true);
                //((CustomVideoView)videoBase).forceResizeToFill(width, height, true);

                videoBase.setOnPreparedListener(null);
            }
        });
        videoBase.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                Log.i("David", "Hemos llegado al onCompletion, con lo cual se ha terminado de cantar. Si no llegamos aquí, no dejaremos subir el vídeo");
                if(prefs==null){
                    prefs=getSharedPreferences(getString(R.string.preferences), Context.MODE_PRIVATE);
                }
                prefs.edit().putBoolean("streamfinalizado", true).apply();
                stopRecording();

            }
        });
        videoBase.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                //stopRecording();
                Log.i("David", "videobase onErrorListener");
                mainModel.showError(getString(R.string.error),getString(R.string.error_video_base));
                stopRecording();
                stopVideos();
                return true;
            }
        });
    }

    private void iniciarCamara() {
        fdCamera = new FDCamera(this);
        // Register cameraResponse to listen 'surfaceCreated' event from camera
        fdCamera.registerCameraResponse(this);
    }

    private Integer getDeviceWidth() {
        WindowManager wm = (WindowManager) GrabacionVideoActivity.this.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        int width=display.getWidth();
        return width;

    }


    private void synchronizedRecord(boolean recording) {
        if (recording) {
            startRecording();
        } else {
            stopRecording();
        }
    }

    public void goBack(View view) {

        deleteFiles();
        Intent intent = new Intent(this, ListaCanciones.class);
        intent.putExtra("idUsuario", idUsuario);
        startActivity(intent);
        finish();

    }


    private void startRecording() {
        fdCamera.startCamera();
        initVideos();
        onStartRecording();
    }

    public void onStartRecording() {
        Log.i("David", "método onStartRecording, en GrabacionVideoActivity");
        Log.i("David", "videoBaseReady? "+isVideoBaseReady);
        //Log.i("David", "videoTextReady? "+isVideoTextReady);
        Log.i("David", "Tenemos auriculares enchufados?");
        AudioManager am1 = (AudioManager)getSystemService(Context.AUDIO_SERVICE);
        Log.i("David", String.valueOf(am1.isWiredHeadsetOn()));
        if(prefs==null){
            prefs=getSharedPreferences(getString(R.string.preferences), Context.MODE_PRIVATE);
        }
        prefs.edit().putBoolean("auriculares", am1.isWiredHeadsetOn()).apply();
        if (isVideoBaseReady/* && isVideoTextReady*/) {
            playVideos();
        } else {
            Log.i("David", "Botón de grabar visible por fallo en método onStartRecording()");
            btRecord.setVisibility(View.VISIBLE);
            btStop.setVisibility(View.GONE);
            mainModel.showError(getString(R.string.error),getString(R.string.error_video_not_available));
        }
    }

    private void stopRecording(){


        pauseVideos();
    }

    private void playVideos() {
        Log.i("David", "En método playVideos.");
        Log.i("David", "videoBaseReady? "+ isVideoBaseReady);
        Log.i("David", "videotextReady? "+isVideoTextReady);
        if (isVideoBaseReady && isVideoTextReady) {
            Log.i("David", "Entramos en el if");
            videoText.setVisibility(View.VISIBLE);

            fdCamera.startRecordVideo();
            videoBase.start();
            videoText.start();
            btRecord.setVisibility(View.GONE);
            btStop.setVisibility(View.VISIBLE);
            btCamera.setVisibility(View.GONE);
            btGoPlay.setVisibility(View.GONE);
            btBack.setVisibility(View.GONE);

            //fdCamera.startRecordVideo();
            /*final Timer timerRec=new Timer();
            timerRec.schedule(new TimerTask() {
                @Override
                public void run() {
                    if(videoBase.isPlaying()){
                        Log.i("David", "Ya estamos reproduciendo");
                        timerRec.cancel();
                        *//*fdCamera.startRecordVideo();
                        videoText.start();*//*
                        timeEnd=System.currentTimeMillis();
                    }else{
                        //Log.i("David", "Aún no estamos reproduciendo");
                    }
                }
            },0,1);*/



        } else {
            mainModel.showError(getString(R.string.error),getString(R.string.error_video_not_available));
        }
    }

    public void upload(View view) {
        /*if (compareVideoDuration()) {*/
            new android.app.AlertDialog.Builder(this)
                    .setTitle(getString(R.string.msg_request_upload_title))
                    .setMessage(getString(R.string.msg_request_upload))
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            onUpload();
                        }
                    })
                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            dialog.cancel();
                        }
                    })
                    .setCancelable(false)
                    .show();
    }

    private void onUpload() {
        if(videoPath==null){
            videoPath= mainModel.getFilePath(FDConstants.APP_FOLDER.FD.toString()) + FDConstants.VIDEO_FILE;
        }

        if (videoPath == null) {
            Toast.makeText(this, "Could not find the filepath of the selected file",
                    Toast.LENGTH_LONG).show();
            return;
        }
        File file = new File(videoPath);

        videoId = new Date().getTime()+"Android"+idUsuario+ FDConstants.VIDEO_SUFFIX;

        TransferObserver observer = transferUtility.upload(AmazonS3Util.BUCKET_NAME, videoId, file);
        TransferListener listener = new TransferListener() {
            @Override
            public void onStateChanged(int id, TransferState state) {
                if (state == TransferState.COMPLETED) {

                    Log.i("David", "Canción subida con éxito");
                    fijarVariableNoPuedeVolverASubirVideo();
                    uploaded();
                }
            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                Log.i("David", id + " bytesCurrent: " + bytesCurrent + " bytesTotal: " + bytesTotal);
            }

            @Override
            public void onError(int id, Exception ex) {
                mainModel.hideProgress();
                mainModel.showError(getString(R.string.error), getString(R.string.error_upload));
            }
        };
        observer.setTransferListener(listener);
        mainModel.showProgress(this, true);

    }

    private void uploaded() {
        /*
        mainModel.showInfo(
                getString(R.string.msg_uploaded_title),
                getString(R.string.msg_uploaded)
        );*/

        //mainModel.hideProgress();

        //Se llama al servicio para Enviar a la BD.
        enviarDatosCancionABaseDatos();
    }

    public void enviarDatosCancionABaseDatos (){

        SharedPreferences sharedPref = getApplicationContext().getSharedPreferences("SubidaCancion",1);
        String identificadorCancion = sharedPref.getString("idCancion","");

        String urlVideBaseOriginal = "http://"+cancionElegida.getUrlVideoBase();

        UploadVideo datosVideo = new UploadVideo();
        datosVideo.setIdUsuario(idUsuario);
        datosVideo.setIdCancion(identificadorCancion);
        datosVideo.setUrlCancionOrig(urlVideBaseOriginal);
        //PARA FORMAR LA URL CANCION USER HAY QUE CONCATENAR AL ID EL BUCKET_NAME
        datosVideo.setUrlCancionUser(videoId);
        datosVideo.setEstado1("1");

        if(!cancionSubida){
            RequestInterface serviceUploadVideo= ApiCliente.getService().create(RequestInterface.class);
            Call<UploadVideoResponse> uploadVideoResponse = serviceUploadVideo.uploadVideo(datosVideo);

            uploadVideoResponse.enqueue(new Callback<UploadVideoResponse>() {
                @Override
                public void onResponse(Call<UploadVideoResponse> call, Response<UploadVideoResponse> response) {
                    Log.i("FD", "Codigo Respuesta " +response.code());
                    UploadVideoResponse uploadVideoResponse = response.body();
                    if (response.isSuccess()) {
                        mainModel.hideProgress();
                        Log.i("FD","Código: "+uploadVideoResponse.getVideo());
                        comprobarCodigoServidor(uploadVideoResponse.getVideo());
                    }else {
                        mainModel.hideProgress();
                        Log.i("FD","Error: "+response.message());
                        mostrarAlerta(getString(R.string.error_conectar_servicio), true);
                    }
                }

                @Override
                public void onFailure(Call<UploadVideoResponse> call, Throwable t) {
                    mainModel.hideProgress();
                    Log.i("FD","Error: " +t.getMessage());
                    mostrarAlerta(getString(R.string.error_conectar_servicio), true);
                }
            });
        }


    }

    public void comprobarCodigoServidor(String codigo){

        switch (codigo){

            case UploadVideoResponse.UPLOAD_VIDEO_OK:
                mostrarAlerta(getString(R.string.videoSubidoCorrectamente), false);
                deleteFiles();
                break;

            case UploadVideoResponse.UPLOAD_VIDEO_DUPLICATED:
                mostrarAlerta(getString(R.string.errorAlSubirVideo), true);
                cancionSubida=true;
                break;

            case UploadVideoResponse.UPLOAD_VIDEO_NOTSONG:
                mostrarAlerta(getString(R.string.errorAlSubirVideo), true);
                break;

            case UploadVideoResponse.UPLOAD_VIDEO_NOTUSER:
                mostrarAlerta(getString(R.string.errorAlSubirVideo), true);
                break;

            default:
                mostrarAlerta(getString(R.string.errorAlSubirVideo), true);
        }

    }

    public void mostrarAlerta(String mensaje, final boolean esAlertaError){

        new android.app.AlertDialog.Builder(this)
                .setTitle(getString(R.string.info))
                .setMessage(mensaje)
                .setNeutralButton(android.R.string.ok,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                if(esAlertaError){

                                    dialog.cancel();

                                }else{

                                    dialog.cancel();
                                    //Nos vamos a la pantalla de grabacion
                                    goToActivityListaCanciones();
                                }

                            }
                        }
                )
                .setCancelable(false)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    public void goToActivityListaCanciones(){

        Intent intent = new Intent(this, ListaCanciones.class);
        intent.putExtra("idUsuario", idUsuario);
        startActivity(intent);
        finish();

    }

    public void fijarVariableNoPuedeVolverASubirVideo(){
        SharedPreferences sharedPref = getApplicationContext().getSharedPreferences("SubidaCancion",1);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean("subirCancion",false);
        editor.apply();
    }

    private void pauseVideos() {
        videoBase.pause();
        //videoText.pause();
        fdCamera.stopRecordVideo();
        final Timer camera = new Timer();
        camera.schedule(new TimerTask() {
            @Override
            public void run() {
                if (fdCamera.isCamaraParada()) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            btGoPlay.setVisibility(View.VISIBLE);
                            btBack.setVisibility(View.VISIBLE);
                            btCamera.setVisibility(View.VISIBLE);
                            btStop.setVisibility(View.GONE);
                            btRecord.setVisibility(View.VISIBLE);
                        }
                    });

                    camera.cancel();
                }
            }
        }, 0, 200);
        videoBase.seekTo(0);

    }

    private void stopVideos() {
        videoBase.stopPlayback();
        videoText.stopPlayback();
        pauseVideos();
    }

    private void detenerSinSubir() {
        videoBase.pause();
        initVideos();
        fdCamera.stopRecordVideo();

        final Timer camera=new Timer();
        camera.schedule(new TimerTask() {
            @Override
            public void run() {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            //btGoPlay.setVisibility(View.VISIBLE);
                            btBack.setVisibility(View.VISIBLE);
                            btCamera.setVisibility(View.VISIBLE);
                            btStop.setVisibility(View.GONE);
                            btRecord.setVisibility(View.VISIBLE);
                        }
                    });

                    camera.cancel();

            }
        }, 0, 200);
        videoBase.seekTo(0);
    }

    private void initVideos() {

        String urlVideoTexto = "http://"+cancionElegida.getUrlVideoTexto();

        //A veces da aquí false, pero el archivo existe...raro raro...
        if(fileArtist.exists()){
            videoBase.setVideoURI(Uri.fromFile(fileArtist));
            if(cancionElegida.isDescargarTexto()){
                videoText.setVideoURI(Uri.fromFile(fileText));
                videoText.seekTo(0);
            }
            videoBase.seekTo(0);
        }else if(!isDownloading && downloadNumber==0){//Se puede probar a preguntar si el vídeo se está reproduciendo, para evitar la descarga superflua.
            isDownloading=true;
            downloadArtist();
        }


    }

    public void goToPlay(View view) {
        Intent intent = new Intent(this, ReproducirVideoGrabadoActivity.class);
        intent.putExtra("cancionSeleccionada",cancionElegida);
        intent.putExtra("usuario",idUsuario);
        startActivity(intent);
    }

    @Override
    public void onSurfaceCreated() {
        resizePreview();
    }

    private void resizePreview() {
        Camera.Size size = fdCamera.getPreviewSize();

        // landscape
        float ratio = (float)size.width/size.height;

        int new_width=0, new_height=0;
        if(videoResultLayout.getWidth()/ videoResultLayout.getHeight()<ratio){
            new_width = Math.round(videoResultLayout.getHeight()*ratio);
            new_height = videoResultLayout.getHeight();
        }else{
            new_width = videoResultLayout.getWidth();
            new_height = Math.round(videoResultLayout.getWidth()/ratio);
        }
        videoResultLayout.setLayoutParams(new FrameLayout.LayoutParams(new_width, new_height));
        //videoResultLayout.setScaleX(-1);
        fdCamera.startCamera();
    }



    public void changeCamera(View view) {
        fdCamera.changeCamera();
        fdCamera.startCamera();
    }

    private void showControls() {
        Log.i("David", "Botón de grabar visible en método showControls");
        btCamera.setVisibility(View.VISIBLE);
        btRecord.setVisibility(View.VISIBLE);
        btBack.setVisibility(View.VISIBLE);
        //btGoPlay.setVisibility(View.GONE);
        if(pDialog.isShowing()){
            pDialog.dismiss();
        }
        /*if(!alertMostrado){
            if(aDialog==null){
                aDialog=new android.app.AlertDialog.Builder(GrabacionVideoActivity.this);
            }
            aDialog.setTitle(getString(R.string.aviso));
            aDialog.setMessage(getString(R.string.artistnotshowing));
            aDialog.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            Log.i("David", "Mostramos el diálogo de la línea 828");
            alertMostrado=true;
            aDialog.show();
        }*/
    }

    private void deleteFiles(){
        Log.i("David", "DeleteFiles");
        if(fileArtist!=null){
            if(fileArtist.exists()){
                fileArtist.delete();
            }
        }else{
            if(folder==null){
                folder=new File(Environment.getExternalStorageDirectory().toString() + "/.FDVIDEOS/");
            }
            fileArtist=new File(folder,".videopruebaartista.mp4");
            if(fileArtist.exists()){
                fileArtist.delete();
            }
        }
        if(fileText!=null){
            if(fileText.exists()){
                fileText.delete();
            }
        }else{
            if(folder==null){
                folder=new File(Environment.getExternalStorageDirectory().toString() + "/.FDVIDEOS/");
            }
            fileText=new File(folder, ".videotext.mp4");
            if(fileText.exists()){
                fileText.delete();
            }
        }
    }

    @Override
    protected void onPause() {
        Log.i("David", "onPause");
        stopVideos();
        //stopRecording();
        StatsManage.pauseActivity();
        super.onPause();
        if(pDialog!=null){
            if(pDialog.isShowing()){
                Log.i("David", "Eliminando ProgressDialog");
                pDialog.dismiss();
            }
        }
        isVideoBaseReady=false;
        isVideoTextReady=false;
        videoPreparado=false;
        videoTextPreparado=false;

    }

    @Override
    protected void onResume()
    {
        if(!videoPreparado){
            prepararVideoBase();
        }
        if(!videoTextPreparado){
            prepararVideoText();
        }
        if(videoPath==null){
            videoPath= mainModel.getFilePath(FDConstants.APP_FOLDER.FD.toString()) + FDConstants.VIDEO_FILE;
        }
        if(file==null){
            file = new File(videoPath);
        }
        if(file.exists()){
            file.delete();
        }

        //videoBase.seekTo(0);
        Log.i("David", "onResume");
        StatsManage.resumeActivity( this );
        super.onResume();
    }

    @Override
    protected void onDestroy()
    {
        Log.i("David", "onDestroy");
        StatsManage.appClose();
        super.onDestroy();
        //deleteFiles();
    }

    private class DownloadListener implements TransferListener {
        @Override
        public void onError(int id, Exception e) {
            Log.e("David", "onError: " + id, e);
        }

        @Override
        public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
            Log.d("David", String.format("onProgressChanged: %d, total: %d, current: %d",
                    id, bytesTotal, bytesCurrent));
            if(getProgressText(bytesCurrent,bytesTotal)!=0){
                pDialog.setProgress(getProgressText(bytesCurrent,bytesTotal));
            }

        }

        @Override
        public void onStateChanged(int id, TransferState state) {
            Log.d("David", "onStateChanged: " + id + ", " + state);

            if(state.toString().equals("COMPLETED")){

                Log.i("David", "Vídeo del texto descargado");
                if(pDialog.isShowing()){
                    pDialog.dismiss();
                }
                /*if(!alertMostrado){
                    if(aDialog==null){
                        aDialog=new android.app.AlertDialog.Builder(GrabacionVideoActivity.this);
                    }
                    aDialog.setTitle(getString(R.string.aviso));
                    aDialog.setMessage(getString(R.string.artistnotshowing));
                    aDialog.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    Log.i("David", "Mostramos el diálogo de la línea 987");
                    alertMostrado=true;
                    aDialog.show();
                }*/
                initVideosAfterDownload();
            }
            if(state.toString().equals("FAILED")){
                Log.i("David", "Descarga fallada");
                if(pDialog.isShowing()){
                    pDialog.dismiss();
                    AlertDialog.Builder aDialog=new AlertDialog.Builder(GrabacionVideoActivity.this);
                    aDialog.setTitle(getString(R.string.alert));
                    aDialog.setMessage(getString(R.string.errordownload));
                    aDialog.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            finish();
                        }
                    });
                    aDialog.show();
                }
            }
        }
    }

    private void downloadArtist() {
        downloadNumber=1;
        transferUtility= AmazonS3Util.getTransferUtility(GrabacionVideoActivity.this);
        folder=new File(Environment.getExternalStorageDirectory().toString() + "/.FDVIDEOS/");
        if(!folder.exists()){
            folder.mkdirs();
        }
        File noMedia=new File(folder, ".nomedia");
        if(!noMedia.exists()){
            try {
                noMedia.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
                Log.i("David", "Fallo creando archivo .nomedia");
            }
        }
        fileArtist=new File(folder,".videopruebaartista.mp4");
        try {
            if(!fileArtist.exists()){
                fileArtist.createNewFile();
            }
        } catch (IOException e) {
            e.printStackTrace();
            Log.i("David", "Fallo creando archivo fileArtist");
        }
        TransferObserver observer=transferUtility.download(AmazonS3Util.BUCKET_DOWNLOAD, cancionElegida.getUrlVideoBase(), fileArtist);
        observer.setTransferListener(new DownloadListenerArtist());
    }

    private class DownloadListenerArtist implements TransferListener {
        @Override
        public void onError(int id, Exception e) {
            Log.e("David", "onError: " + id, e);
        }

        @Override
        public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
            Log.d("David", String.format("onProgressChanged: %d, total: %d, current: %d",
                    id, bytesTotal, bytesCurrent));
            pDialog.setProgress(getProgress(bytesCurrent,bytesTotal));
        }

        @Override
        public void onStateChanged(int id, TransferState state) {
            Log.d("David", "onStateChanged: " + id + ", " + state);
            if(state.toString().equals("COMPLETED")){

                Log.i("David", "Vídeo del artista descargado");

                isVideoBaseReady=true;
                if(cancionElegida.isDescargarTexto()){
                    downloadText();
                }else{
                    initVideosAfterDownload();
                    if(pDialog.isShowing()){
                        pDialog.dismiss();
                    }
                }

            }
            if(state.toString().equals("FAILED")){
                isDownloading=false;
                if(pDialog.isShowing()){
                    pDialog.dismiss();
                    AlertDialog.Builder aDialog=new AlertDialog.Builder(GrabacionVideoActivity.this);
                    aDialog.setTitle(getString(R.string.alert));
                    aDialog.setMessage(getString(R.string.errordownload));
                    aDialog.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            deleteFiles();
                            dialog.dismiss();
                            finish();
                        }
                    });
                    aDialog.show();
                }
            }
        }
    }

    private void downloadText() {

        folder=new File(Environment.getExternalStorageDirectory().toString() + "/.FDVIDEOS/");
        if(!folder.exists()){
            folder.mkdirs();
        }
        File noMedia=new File(folder, ".nomedia");
        if(!noMedia.exists()){
            try {
                noMedia.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
                Log.i("David", "Fallo creando archivo .nomedia");
            }
        }
        fileText=new File(folder,".videotext.mp4");
        try {
            if(!fileText.exists()){
                fileText.createNewFile();
            }
        } catch (IOException e) {
            e.printStackTrace();
            Log.i("David", "Fallo creando archivo videotext");
        }
        TransferObserver observer=transferUtility.download(AmazonS3Util.BUCKET_DOWNLOAD, cancionElegida.getUrlVideoTexto(), fileText);
        observer.setTransferListener(new DownloadListener());
    }

    private void initVideosAfterDownload() {
        videoBase.setVideoURI(Uri.fromFile(fileArtist));
        videoBase.seekTo(0);
        if(cancionElegida.isDescargarTexto()){
            videoText.setVideoURI(Uri.fromFile(fileText));
            videoText.seekTo(0);
        }


    }

    private int getProgress(long bytesCurrent, long bytesTotal) {
        Long percent=0L;
        if(bytesTotal>0){
            percent=bytesCurrent*75/bytesTotal;
            Log.i("David", "Percent devuelto: "+percent);

        }
        return percent.intValue();
    }

    private int getProgressText(long bytesCurrent, long bytesTotal){
        Long percent=0L;
        if(bytesTotal>0){
            percent=75+(bytesCurrent*25/bytesTotal);
            Log.i("David", "Percent del texto devuelto: "+percent);
        }
        return percent.intValue();
    }

}
